# LovliPlanet
# Back-end
Le développement côté serveur a été fait entierement en Nodejs, cette partie comporte la gestion des APIs liées à l'application mobile et à la mise en place de la base de données; MongoDB.<br/>

## Base de données : MongoDB

Le base de données utilisées est MongoDB, comme expliqué dans la section **lovliplanet**.<br/>
Le développement de la base de données est réalisé dans la section **server/models**.<br/>
Cette application comporte 5 documents (tables) qui sont:

*  **User**: Schema des utilisateurs de l'application
*  **Event**: Schema des évènements de l'application
*  **Theme**: Schema des thèmes de l'application
*  **Notifications**: Schema des notifications de l'application
*  **Comments**: Schema des commentaires de l'application

## APIs LovliPlanet
**La documentation de l'API de l'application se trouve [ici](<https://lovliplanet-api.decoderoom.ovh/api-docs>)**