var express = require("express");
var fetch = require("node-fetch");
var router = express.Router();

const AUTH_API = "https://lovli-auth-api.decoderoom.ovh/v1/auth";
const REALM = "lovliplanet";
const MAIN_REALM = "auth_master";

/* GET home page. */
router.get("/changePassword/:email", (req, res) =>
  res.render("changePassword", {
    email: req.params.email,
    message: ""
  })
);

router.post("/changePassword/:email", (req, res) => {
  const email = req.params.email;
  const { oldPassword, newPassword } = req.body;

  fetch(`${AUTH_API}/${REALM}?email=${email}`, {
    method: "post",
    body: JSON.stringify({
      pwd: oldPassword
    }),
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(resp => resp.json())
    .then(json => {
      if (json.llt_token === undefined || json.llt_token === null) {
        res.render("changePassword", {
          email: email,
          message: "Ancien mot de passe incorrecte"
        });
      } else {
        fetch(`${AUTH_API}/${email}/change`, {
          method: "post",
          body: JSON.stringify({
            llt_token: json.llt_token,
            pwd: newPassword
          }),
          headers: {
            "Content-Type": "application/json"
          }
        }).then(resp => {
          //console.table(json.llt_token);
          if (resp.ok) {
            res.render("notification", {
              title: "Mot de passe modifié",
              message: "Votre mot de passe a été modifié avec succès."
            });
          } else {
            res.render("notification", {
              title: "404 Error",
              message:
                "Un erreur est survenue lors de la connexion avec le serveur de LovliPlanet. Veuillez réessayer plus tard."
            });
          }
        });
      }
    })
    .catch(err => console.log(err));
});

router.get("/emailVerified", (req, res) =>
  res.render("notification", {
    title: "Compte activé",
    message:
      "Votre compte est activé, vous pouvez désormais vous connecter à l 'application LovliPlanet."
  })
);

module.exports = router;
