const express = require('express');
const controller = require('../controllers/users');

const router = express.Router();

/* POST users creating. */
router.post('/', controller.createUser);

/* GET users listing. */
router.get('/', controller.getUsers);

/* GET user listing. */
router.get('/:userId', controller.getUser);

/* PUT users updating. */
router.put('/:userId', controller.updateUser);

/* POST users deleting. */
router.delete('/:userId', controller.deleteUser);

/* POST users notification pushing. */
router.post('/:userId/notifications', controller.pushNotification);

/* PUT users notification viewed. */
router.put('/:userId/notifications/:notifId', controller.viewedNotification);

module.exports = router;