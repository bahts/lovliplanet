const express = require('express');
const controller = require('../controllers/events');

const router = express.Router();

/* POST events creating. */
router.post('/', controller.createEvent);

/* GET events listing. */
router.get('/', controller.getEvents);

/* GET event listing. */
router.get('/:eventId', controller.getEvent);

/* PUT events updating. */
router.put('/:eventId', controller.updateEvent);

/* POST events deleting. */
router.delete('/:eventId', controller.deleteEvent);

/* POST comments event pushing. */
router.post('/:eventId/comments', controller.addComment);

/* PUT comments event updating. */
router.put('/:eventId/comments/:commentId', controller.updateComment);

/* DELETE comments event updating. */
router.delete('/:eventId/comments/:commentId', controller.deleteComment);

/* PUT comments event updating. */
router.put('/:eventId/handleViews/:userId', controller.updateNbViews);

/* PUT comments event updating. */
router.put('/:eventId/handleParticipants/:userId', controller.updateNbParticipants);

module.exports = router;