var express = require('express');
var router = express.Router();

const Users = require('../models/user').Users;

/* GET users listing. */
router.post('/', function (req, res) {
    let {
        email
    } = req.body;
    Users.find({
            email: email
        }).then(users => {
            res.status(200).json(users)
        })
        .catch(err => res.send(err));
});

module.exports = router;