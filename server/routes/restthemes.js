const express = require('express');
const controller = require('../controllers/themes');

const router = express.Router();

/* POST themes creating. */
router.post('/', controller.createTheme);

/* GET themes listing. */
router.get('/', controller.getThemes);

/* GET themes listing. */
router.get('/:themeId', controller.getTheme);

/* PUT themes updating. */
router.put('/:themeId', controller.updateTheme);

/* DELETE themes deleting. */
router.delete('/:themeId', controller.deleteTheme);

module.exports = router;