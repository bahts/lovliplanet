class IoController {

    constructor() {}

    registerEventSocket(socket) {
        console.log(`Connection to events done by ${socket.id}`);
        socket.on('create', data => {
            socket.broadcast.emit('new_event', data);
        });
        socket.on('edit', data => {
            socket.broadcast.emit('edit_event', data);
        });
        socket.on('delete', data => {
            socket.broadcast.emit('delete_event', data);
        });
        socket.on('comment', data => {
            socket.broadcast.emit('new_comment', data);
        });
    }

    registerThemeSocket(socket) {
        console.log(`Connection to themes done by ${socket.id}`);
        socket.on('create', (data) => {
            socket.broadcast.emit('new_theme', data);
        });
        socket.on('edit', (data) => {
            socket.broadcast.emit('edit_theme', data);
        });
        /*
        socket.on('delete', (data) => {
            socket.broadcast.emit('delete_theme', data);
        }); */
    }

}

module.exports = new IoController();