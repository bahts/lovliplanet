const Themes = require('../models/theme').Themes;

/*
 * Cette fonction est appélé lorsqu'un thème veut être créé
 */
const createTheme =
    (req, res) => {
        let newTheme = {
            ...req.body
        };
        Themes.create(newTheme)
            .then(theme => res.status(200).json(theme))
            .catch(err => res.send(err));
    }

/*
 * Cette fonction renvoie tous les thèmes de la base
 */
const getThemes =
    (req, res) => {
        Themes.find()
            .then(themes => res.status(200).json(themes))
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction renvoie un thème correspondant à un id donné
 */
const getTheme =
    (req, res) => {
        Themes.findById(req.params.themeId)
            .then(theme => res.status(200).json(theme))
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction modifie un thème correspondant à un id donné
 */
const updateTheme =
    (req, res) => {
        let updatedTheme = {
            ...req.body
        };
        Themes.findByIdAndUpdate(req.params.themeId, updatedTheme, {
                new: true
            }).then(theme => res.status(200).json(theme))
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction supprime un thème correspondant à un id donné
 */
const deleteTheme =
    (req, res) => {
        Themes.findByIdAndRemove(req.params.themeId)
            .then(() => res.status(200).end())
            .catch(err => res.send(err));
    }

// Exporter les fonctions pour être utilisées dans /routes/restthemes
module.exports.createTheme = createTheme;
module.exports.getThemes = getThemes;
module.exports.getTheme = getTheme;
module.exports.updateTheme = updateTheme;
module.exports.deleteTheme = deleteTheme;