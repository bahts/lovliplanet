const mongoose = require('mongoose');
const mongodb = require('mongodb');
const mongodbUri = require('mongodb-uri');

///////// MongoDB configuration ////////////

const MongoClient = mongodb.MongoClient;

let mongoUri = process.env.MONGO_URI || 'mongodb://localhost/dblovliplanet';
let mongoUriObj = mongodbUri.parse(mongoUri);
mongoUriObj.hosts[0].port = mongoUriObj.hosts[0].port || 27017;

const db = {
    url: `mongodb://${mongoUriObj.hosts[0].host}:${mongoUriObj.hosts[0].port}`,
    dbName: mongoUriObj.database
}

main = async () => {
    try {
        mgoclient = await MongoClient.connect(db.url, {
            useNewUrlParser: true
        });

        /*
        globals.contact_collection = mgoclient.db(db.db_name).collection('contact')
        app.context.db = globals.contact_collection

        const server = await app.listen(process.env.PORT || '8000', () => {
            console.log('Mongo URI', mongoUri)
            console.log('Mongo computed URI', db.url + '/' + db.db_name)
            console.log('App listening on port %s', server.address().port)
            console.log('')
        })
        */

    } catch (e) {
        console.error('Error:', e);
    }
}

// run main
main().catch(e => {
    console.error('Error:', e)
});

///////// Mongoose configuration ////////////

mongoose.Promise = global.Promise;
// URI de la base de donnée
const dbURI = mongoUri;
// se connecter a la base de donnée
const dbConnection = mongoose.createConnection(dbURI, {
    useCreateIndex: true,
    useNewUrlParser: true
});

// exporter la connection créee
module.exports = dbConnection;

dbConnection.on('connected', () => console.log('db.js: connected to ' + dbURI));
dbConnection.on('disconnected', () => console.log('db.js: disconnected from ' + dbURI));
dbConnection.on('error', err => console.log('db.js: connection error ' + err));

//
// "clean"  management of connection end
//
const shutdown = function (msg, callback) {
    dbConnection.close(() => {
        console.log(' Mongoose shutdown : ' + msg);
        callback();
    });
}

// application killed (ctrl+c)
process.on('SIGINT', () => shutdown('application ends', () => process.exit(0)));
// for nodemon
process.once('SIGUSR2', () => shutdown('nodemon restarting', () => process.kill(process.pid, 'SIGUSR2')));
// process killed (POSIX)
process.on('SIGTERM', () => shutdown('SIGTERM received', () => process.exit(0)));