const Events = require('../models/event').Events;

/*
 * Cette fonction est appélé lorsqu'un evenement veut être créée
 */
const createEvent =
    (req, res) => {
        let newEvent = {
            ...req.body
        };
        Events.create(newEvent)
            .then(event => res.status(200).json(event))
            .catch(err => res.send(err));
    }

/*
 * Cette fonction renvoie tous les evenements de la base
 */
const getEvents =
    (req, res) => {
        const {
            page,
            limit
        } = req.query;
        Events.find()
            .sort({
                _id: -1
            })
            .skip(parseInt(page))
            .limit(parseInt(limit))
            .then(events => res.status(200).json(events))
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction renvoie un evenement correspondant à un id donné
 */
const getEvent =
    (req, res) => {
        Events.findById(req.params.eventId)
            .then(event => res.status(200).json(event))
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction modifie un evenement correspondant à un id donné
 */
const updateEvent =
    (req, res) => {
        let updatedEvent = {
            ...req.body
        };
        Events.findByIdAndUpdate(req.params.eventId, updatedEvent, {
                new: true
            }).then(event => {
                res.status(200).json(event)
            })
            .catch(err => res.send(err));
    }
/* 
 * Cette fonction supprime un evenement correspondant à un id donné
 */
const deleteEvent =
    (req, res) => {
        Events.findByIdAndRemove(req.params.eventId)
            .then(() => res.status(200).end())
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction permet d'ajouter un commentaire donné à un evenement donné
 */
const addComment =
    (req, res) => {
        let newComment = {
            ...req.body
        };
        Events.findByIdAndUpdate(req.params.eventId, {
                $push: {
                    comments: newComment
                }
            }, {
                new: true
            })
            .then(event => {
                res.status(200).json(event)
            })
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction permet de supprimer un commentaire donné à un evenement donné
 */
const deleteComment =
    (req, res) => {
        Events.findByIdAndUpdate(req.params.eventId, {
                $pull: {
                    comments: {
                        _id: commentId
                    }
                }
            }, {
                new: true
            })
            .then(comments => res.status(200).json(comments))
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction permet de modifier un commentaire donné d'un évènement donné
 */
const updateComment =
    (req, res) => {
        let newComment = {
            ...req.body
        };
        Events.update({
                _id: req.params.eventId,
                'comments._id': req.params.commentId
            }, {
                $set: {
                    'comments.$.content': newComment.content
                }
            }, {
                new: true
            }).then(comment => res.status(200).json(comment))
            .catch(err => res.send(err));
    }

/*
 *
 */
const updateNbViews =
    async (req, res) => {
        let event = await Events.findById(req.params.eventId);
        event.viewers.push(req.params.userId);
        event.save(req.params.userId)
            .then(() => res.status(200).json(event))
            .catch(err => res.send(err));

    }

const updateNbParticipants =
    async (req, res) => {
        let {
            willParticipate
        } = {
            ...req.body
        };
        let event = await Events.findById(req.params.eventId);
        willParticipate ?
            event.participants.push(req.params.userId) :
            event.participants.splice(event.participants.indexOf(req.params.userId), 1);
        event.save()
            .then(() => res.status(200).json(event))
            .catch(err => res.send(err));
    }

// Exporter les fonctions pour être utilisées dans /routes/restevents
module.exports.createEvent = createEvent;
module.exports.getEvent = getEvent;
module.exports.updateEvent = updateEvent;
module.exports.deleteEvent = deleteEvent;
module.exports.addComment = addComment;
module.exports.deleteComment = deleteComment;
module.exports.updateComment = updateComment;
module.exports.getEvents = getEvents;
module.exports.updateNbViews = updateNbViews;
module.exports.updateNbParticipants = updateNbParticipants;