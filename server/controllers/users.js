const Users = require('../models/user').Users;

/*
 * Cette fonction est appélé lorsqu'un utilisateur veut être créée
 */
const createUser =
    (req, res) => {
        let newUser = {
            ...req.body
        };
        Users.create(newUser)
            .then(user => res.status(200).json(user))
            .catch(err => res.send(err));
    }

/*
 * Cette fonction renvoie tous les utilisateurs de la base
 */
const getUsers =
    (req, res) => {

        Users.find()
            .then(users => res.status(200).json(users))
            .catch(err => res.send(err));

    }


/* 
 * Cette fonction renvoie un utilisateur correspondant à un id donné
 */
const getUser =
    (req, res) => {
        Users.findById(req.params.userId)
            .then(user => res.status(200).json(user))
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction modifie un utilisateur correspondant à un id donné
 */
const updateUser =
    (req, res) => {
        let updatedUser = {
            ...req.body
        };
        Users.findByIdAndUpdate(req.params.userId, updatedUser, {
                new: true
            }).then(user => res.status(200).json(user))
            .catch(err => res.send(err));
    }


/* 
 * Cette fonction supprime un utilisateur correspondant à un id donné
 */
const deleteUser =
    (req, res) => {
        Users.findByIdAndRemove(req.params.userId)
            .then(() => res.status(200).end())
            .catch(err => res.send(err));
    }

/* 
 * Cette fonction permet d'emettre une notification à un utilisateur correspondant à un id donné
 */
const pushNotification =
    (req, res) => {
        let newNotif = {
            ...req.body
        };
        console.table(newNotif);
        Users.findByIdAndUpdate(req.params.userId, {
                $push: {
                    notifications: newNotif
                }
            }, {
                new: true
            })
            .then(user => {
                console.log(user);
                res.status(200).json(user);
            })
            .catch(err => res.send(err));
    }

/*
 * Cette fonction permet marquer une notification comme vue
 */
const viewedNotification =
    (req, res) => {
        Users.update({
                _id: req.params.userId,
                'notifications._id': req.params.notifId
            }, {
                $set: {
                    'notifications.$.viewed': true
                }
            }, {
                new: true
            }).then(notification => res.status(200).json(notification))
            .catch(err => res.send(err));
    }

// Exporter les fonctions pour être utilisées dans /routes/restusers
module.exports.createUser = createUser;
module.exports.getUsers = getUsers;
module.exports.getUser = getUser;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;
module.exports.pushNotification = pushNotification;
module.exports.viewedNotification = viewedNotification;