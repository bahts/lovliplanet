const mongoose = require("mongoose");
const dbConnection = require("../controllers/db");
const commentSchema = require("./comment");

const ObjectId = mongoose.Schema.Types.ObjectId;

//const DEFAULT_COVER = convert.base64_encode('http://localhost:3000/images/illustration.png');

const eventSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    theme: {
        type: ObjectId,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    date: {
        type: Number,
        required: true
    },
    author: {
        type: ObjectId,
        required: true
    },
    cover: {
        type: String,
        default: '' //DEFAULT_COVER
    },
    location: {
        type: String
    },
    viewers: {
        type: [ObjectId],
        default: []
    },
    participants: {
        type: [ObjectId],
        default: []
    },
    comments: {
        type: [commentSchema],
        default: []
    },
    createAt: {
        type: Number,
        default: Date.now
    },
});

const Events = dbConnection.model("Event", eventSchema, "events");

module.exports.schema = eventSchema;
module.exports.Events = Events;