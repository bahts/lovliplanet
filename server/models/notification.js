const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;

module.exports = new mongoose.Schema({
    genre: {
        type: String,
        required: true
    }, // savoir à quoi est lié cette notification (event, theme, ...)
    content: {
        type: String,
        required: true
    },
    target: {
        type: ObjectId,
        required: true
    },
    viewed: {
        type: Boolean,
        required: true,
        default: false
    },
    createAt: {
        type: Number,
        set: Date.now
    }
});