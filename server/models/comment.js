const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;

module.exports = new mongoose.Schema({
    event: {
        type: ObjectId,
        required: true
    },
    author: {
        type: ObjectId,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    createAt: {
        type: Number,
        set: Date.now
    }
})