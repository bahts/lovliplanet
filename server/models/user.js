const mongoose = require('mongoose');
const dbConnection = require('../controllers/db');
const notificationSchema = require('./notification');

const ObjectId = mongoose.Schema.Types.ObjectId;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    login: {
        type: String,
        required: true,
        unique: true
    },
    llt_token: {
        type: String,
        default: ""
    },
    jwt_token: {
        type: String,
        default: ""
    },
    avatar: {
        type: String,
        default: ""
    },
    notifications: {
        type: [notificationSchema],
        default: []
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    enableNotification: {
        type: Boolean,
        default: true
    },
    enableReminder: {
        type: Boolean,
        default: true
    },
    createAt: {
        type: Number,
        default: Date.now
    },
});

const Users = dbConnection.model("User", userSchema, "users");

module.exports.schema = userSchema;
module.exports.Users = Users;