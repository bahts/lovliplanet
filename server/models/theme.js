const mongoose = require('mongoose');
const dbConnection = require('../controllers/db');

const ObjectId = mongoose.Schema.Types.ObjectId;

const themeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    cover: {
        type: String,
        default: ''
    },
    subscribers: {
        type: [ObjectId],
        default: []
    },
    nbSubscriptions: Number,
    createBy: ObjectId,
    createAt: {
        type: Number,
        set: Date.now
    }
});

const Themes = dbConnection.model('Theme', themeSchema, 'themes');

module.exports.schema = themeSchema;
module.exports.Themes = Themes;