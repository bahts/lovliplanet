# LovliPlanet

## Présentation

LovliPlanet est une application de gestion d'évènements destinée aux collaborateurs Orange. Cette appilcation fait partie d'un groupe d'applications développer par l'équipe, nommée Lovli, afin de d'acceuillir les collaborateurs dans les meilleurs conditions et améliorer le bien être des salariés et l'ergonomie du bâtiment à l'occasion du démenagement de la direction Nord d'Orange dans de nouveaux locaux.

## Utilisation

#### 1. Récuperer le projet depuis GitLab
- Cloner le projet:
    `$ git clone https://gitlab.forge.orange-labs.fr/coderoom-vda/lovliplanet`
- Mettre à jour le dépôt local (Ouvrir un terminal dépuis le dossier **lovliplanet**): 
    `$ git pull`

#### 2. Lancer le serveur (seulement en local)
1. Accéder au dossier *server*<br/> 
    - Après un *git clone <dépôt>*:
        `$ cd lovliplanet/server`
    - Après *git pull*:
        `$ cd server`
2. Lancer le server<br/>
    `$ npm start`
        
#### 3. Lancer l'application
- Sur un émulateur: voir cette [vidéo](<https://youtu.be/GLSG_Wh_YWc>)
- Sur un smartphone physique: sur [ce lien](<https://flutter.dev/docs/get-started/install/windows>) vous trouverez une explication détaillée

## Technologies utilisées

Ce projet a été développé avec trois technologies principales, qui sont:

### Base de données : MongoDB

Dans un système de base de données relationnelles les données sont stockées par ligne dans des tables. Et il est souvent nécessaire de faire des jointures sur plusieurs tables afin de tirer des informations assez pertinentes de la base. Dans **mongoDB**,les données sont modélisées sous forme de document sous un style **JSON**. On ne parle plus de tables, ni d’enregistrements mais de collections et de documents.<br/>
Ce système de gestion de données nous évite ainsi de faire des jointures de tables car toutes les informations propres à un certain donnée sont stockées dans un même document.<br/>
Les documents sont les unités de base dans une base mongoDB. Ils sont équivalents aux objets JSON et sont comparables aux enregistrements d’une table dans une base de données relationnelle.
Tout document appartient à une collection et a un champ appelé \_id qui identifie le
document dans la base de données. mongoDB enregistre les documents sur le disque sous un format BSON (JSON binaire).

    {
        "_id": ObjectId("54ce14c9dc45f155a233301"),
        "nom": "GoodFelles",
        "acteurs": [
            {
                "nom": "Deniro",
                "prenom": "Robert",
            },
            {
                "nom": "Liota",
                "prenom": "Ray",
            }
        ]
    }

Le JSON est un format de représentation textuelle des données dérivé de la notation des objets du langage JavaScript. Toutefois il est indépendant du JavaScript et de tout autre langage de programmation. Le JSON permet de représenter de l’information structurée comme le permet XML par exemple. Un objet JSON a pour but de représenter des informations (valeurs) accompagnées d'étiquettes (champs) permettant de les identifier.

Un objet JSON ne comprend que deux types d’éléments structurels :

- des ensembles de paires nom/valeurs
- des listes ordonnées de valeurs

Les valeurs représentent trois types de données :

- des objets
- des tableaux
- des valeurs génériques de type tableau, objet, booléen, nombre, chaîne ou null

Les champs par contre ne peuvent être que des chaînes de caractères.

    {
        "name": "sue",
        "age": 26,
        "status": "A",
        "groups": ["news", "sports"]
    }

**Pour plus d'informations concernant l'installation et l'utilisation de MongoDB consulter leur [site officiel](<https://docs.mongodb.com/manual/>)**

### Backend : Nodejs

<div align="center">
    ![Image](images/nodejs.png)
</div>

**Nodejs** est une plateforme de développement Javascript. Ce n’est pas un serveur, ce n’est pas un framework, c’est juste le langage Javascript avec des bibliothèques permettant de réaliser des actions comme écrire sur la sortie standard, ouvrir/fermer des connections réseau ou encore créer un fichier. <br/>
Il est souvent confondu avec un serveur car c’est son origine : **Nodejs** a été créé par Ryan Dahl dans le but de pouvoir créer des applications temps réel où le serveur est capable de pousser de l’information au client. C’est dans ce but qu’il utilise la bibliothèque libuv pour réaliser son modèle d’entrée sortie non bloquante.

    var http = require('http');

    var server = http.createServer(function(req, res) {
      res.writeHead(200);
      res.end('Salut tout le monde !');
    });
    server.listen(8080);

Pour l'installer vous pouver utiliser une des trois méthodes ci-dessous:

1.  Utiliser un gestionnaire de paquets (brew, macport, apt-get, ...)
2.  Utiliser un des binaires disponible sur le site officiel dans le rubrique *téléchargement* de Nodejs
3.  Compiler le code source

Pour accéder à la documentation complète du développement côté serveur, cliquer sur le dossier **server** ci-desuus.

**Pour plus d'informations concernant l'installation et l'utilisation de Nodejs consulter leur [site officiel](<https://nodejs.org/fr/docs/>)**

### Frontend : Flutter

**Flutter** est un framework développé par Google, le plus récent de tous. De ce fait, les ingénieurs ont pu observer les points forts et les faiblesses de chaque outil existant pour n’en extraire que la quintessence.<br/>
Ce nom vous est par ailleurs peut-être familier, puisqu’il s’agit de l’une des briques essentielles de Fuchsia/Andromeda. En effet ce framework est utilisé pour tout ce qui est interface utilisateur. Mais aujourd’hui Flutter se fait surtout connaître pour sa capacité à concevoir des applications natives multiplateforme pour Android et iOS (Windows/Mac/Linux sont également supportés).<br/>
L’équipe de Flutter provient essentiellement du Web (plus particulièrement de Chrome) et a essayé d’adapter sa philosophie au monde du mobile. Ils se sont appuyés pour cela sur Skia, le moteur qui fait partie intégrante de Chrome ou encore de toute la gestion du texte d’Android.

#### Dart comme langage de programmation
Google a créé de nombreux langages de programmation, dont Dart. Celui-ci ne vous dit peut-être rien… et ne nous le cachons pas, il commençait à tomber dans l’oubli. Mais depuis l’émergence de Flutter, ce langage retrouve des couleurs et a même connu une version 2.0.<br/>
Si Google a opté pour Dart, c’est parce qu’il offre deux modes de fonctionnement. Le premier, nommé AOT (pour Ahead Of Time), permet de générer une application native pour chaque plateforme. L’avantage de Flutter, par rapport à ses concurrents, est donc fort, puisque le code sera optimisé directement pour l’architecture sur laquelle il fonctionnera.

<div align="center">
    ![Image](images/flutter-hot-reload.png)
</div>

#### Dart comme langage de programmation
Contrairement à d’autres systèmes, Flutter offre une quantité incroyable de Widgets (plusieurs centaines). L’idée développée par ses concepteurs est qu’un Widget ne doit faire que sa propre tâche.<br/>
Prenons le cas d’un texte : le Widget Text ne sait afficher que du texte à l’écran. Il ne saura pas modifier la couleur de fond ou gérer son emplacement à l’écran. Ces deux autres tâches seront dédiées à d’autres Widgets spécialisés. Ce parti pris est ce qui fait aujourd’hui la force de ce framework, en extrayant des performances incroyables.<br/>
Ce principe permet aussi à Flutter d’être compréhensible par tous. Certes, les développeurs continueront à concevoir des applications, mais on pourrait aisément penser que des designers pourraient s’y intéresser.

<div align="center">
    ![Image](images/flutter.png)
</div>

Voici un exemple, affichant un texte et une icône centrés :

    Center(
        child: Column(
            children: [
                Text('Hello World!'),//Text
                Icon(Icons.star, color: Colors.green) //Icon
            ]
        );//Column
    );//Center
    
Afin de mieux comprendre le fonctionnement de flutter, il serait mieux de connaitre les bases de Dart.

Pour accéder à la documentation complète du développement côté serveur, cliquer sur le dossier **flutter_app** ci-desuus.

**Pour plus d'informations concernant l'installation et l'utilisation de Dart et Flutter consulter la documention d [langage Dart](<https://dart.dev/guides/language/language-tour>) et du [framework Flutter](<https://dart.dev/guides/language/language-tour>)**


