import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import '../screens/main/profil.dart';
import './avatar.dart';

class UserWidget extends StatelessWidget {
  final User user, currentUser;
  final List<Event> events;

  const UserWidget({Key key, this.user, this.currentUser, this.events})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade100,
          ),
        ],
      ),
      child: Card(
        color: Colors.white,
        child: ListTile(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Profil(
                        user: user,
                        currentUser: currentUser,
                        events: events,
                      ),
                ));
          },
          leading: Avatar(
            user: user,
          ),
          title: Text('${user.login}'),
          subtitle: Text(user.isAdmin ? "Administrateur" : "Utilisateur"),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
      ),
    );
  }
}
