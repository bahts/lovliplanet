import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/notification.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/models/theme.dart' as model;
import 'package:flutter_app/screens/main/displayEvents.dart';
import 'package:flutter_app/screens/main/eventDetail.dart';
import 'package:flutter_app/utils/date.dart';
import 'package:flutter_app/widgets/cover.dart';

class NotifWidget extends StatelessWidget {
  final NotificationObj notification;
  final User currentUser;
  final List<Event> events;
  final List<model.Theme> themes;

  const NotifWidget(
      {Key key, this.notification, this.currentUser, this.events, this.themes})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NotifWidgetFul(
        this.notification, this.currentUser, this.events, this.themes);
  }
}

class NotifWidgetFul extends StatefulWidget {
  final NotificationObj notification;
  final User currentUser;
  final List<Event> events;
  final List<model.Theme> themes;

  const NotifWidgetFul(
      this.notification, this.currentUser, this.events, this.themes);

  @override
  State<StatefulWidget> createState() {
    return NotifWidgetState();
  }
}

class NotifWidgetState extends State<NotifWidgetFul> {
  NotificationObj _notification;
  User _currentUser;
  List<Event> _events;
  List<model.Theme> _themes;
  Event _event;
  model.Theme _theme;

  @override
  void initState() {
    super.initState();
    _notification = widget.notification;
    _currentUser = widget.currentUser;
    _events = widget.events;
    _themes = widget.themes;
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    if (_notification.genre == 'event') {
      setState(() {
        _event = _events.firstWhere((event) => event.id == _notification.target,
            orElse: () => _events[0]);
      });
    } else {
      setState(() {
        _theme = _themes.firstWhere((theme) => theme.id == _notification.target,
            orElse: () => _themes[0]);
      });
    }

    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade100,
          ),
        ],
      ),
      child: Card(
        color: _notification.viewed ? Colors.white : Colors.orange.shade50,
        child: ListTile(
          onTap: () {
            if (!_notification.viewed) {
              _notification.viewedNotification(_currentUser.id);
            }
            setState(() {
              _notification.viewed = true;
            });
            (_notification.genre == 'event')
                ? Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => EventDetail(
                            themes: _themes,
                            currentUser: _currentUser,
                            event: _event,
                            events: _events,
                          ),
                    ))
                : Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => DisplayEvents(
                            title: _theme.name,
                            themes: [_theme],
                            currentUser: _currentUser,
                            events: List<Event>.from(_events
                                .where((event) => event.theme == _theme.id)),
                          ),
                    ));
          },
          leading: Cover(
            cover: _notification.genre == 'event' ? _event.cover : _theme.cover,
            defaultCover: _notification.genre == 'event'
                ? 'assets/images/illustration.png'
                : 'assets/images/theme-icon.png',
          ),
          title: Text(_notification.content),
          subtitle: Text("${getTimeFromNow(_notification.createAt)}"),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
      ),
    );
  }
}
