import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/widgets/cover.dart';
import '../models/theme.dart' as model;
import './cover.dart';
import '../screens/main/displayEvents.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';

class ThemeWidget extends StatelessWidget {
  final model.Theme theme;
  final List<model.Theme> themes;
  final User currentUser;
  final List<Event> events;
  final SocketIO themeSocket, eventSocket;

  const ThemeWidget(this.theme, this.currentUser, this.events, this.themeSocket,
      this.eventSocket, this.themes);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: () {
          Navigator.pop(context);
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => DisplayEvents(
                      eventSocket: eventSocket,
                      themeSocket: themeSocket,
                      title: theme.name,
                      theme: theme,
                      themes: themes,
                      currentUser: currentUser,
                      events: List<Event>.from(
                          events.where((event) => event.theme == theme.id)),
                    ),
              ));
        },
        leading: Container(
          margin: EdgeInsets.symmetric(vertical: 2),
          child: Cover(
            defaultCover: 'assets/images/theme-icon.png',
            cover: theme.cover,
          ),
        ),
        title: Text(theme.name),
        trailing: Icon(Icons.keyboard_arrow_right),
      ),
    );
  }
}
