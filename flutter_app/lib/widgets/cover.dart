import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';

class Cover extends StatelessWidget {
  final List<int> cover;
  final String defaultCover;
  final double width;

  const Cover({Key key, this.cover, this.defaultCover, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return cover.length == 0
        ? Image.asset(
            defaultCover,
          )
        : Image.memory(
            cover,
            width: width,
          );
  }
}
