import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/widgets/event.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:flutter_app/models/theme.dart' as model;

class ListEvent extends StatelessWidget {
  final List<Event> events;
  final User currentUser;
  final SocketIO eventSocket;
  final List<model.Theme> themes;
  final ScrollController scrollController;

  const ListEvent(this.events, this.currentUser, this.eventSocket, this.themes,
      this.scrollController);

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Container(
      color: Colors.grey.shade200,
      child: Scrollbar(
        child: ListView.builder(
          padding: EdgeInsets.symmetric(horizontal: SIZE),
          reverse: false,
          controller: scrollController,
          itemBuilder: (_, int index) => EventWidget(
                event: events[index],
                currentUser: currentUser,
                events: events,
                eventSocket: eventSocket,
                themes: themes,
              ),
          itemCount: events.length,
        ),
      ),
    );
  }
}
