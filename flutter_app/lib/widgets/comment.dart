import 'package:flutter/material.dart';
import 'package:flutter_app/models/comment.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/utils/date.dart';
import 'package:flutter_app/widgets/avatar.dart';

class CommentWidget extends StatelessWidget {
  final Comment comment;

  const CommentWidget({Key key, this.comment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return FutureBuilder<User>(
        future: User.getUser(comment.author),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final User author = snapshot.data;
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  child: Avatar(
                    user: author,
                  ),
                  flex: 1,
                ),
                Flexible(
                  flex: 8,
                  child: Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: 2 * SIZE, vertical: SIZE / 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('${author.login}',
                            style: TextStyle(
                              color: Colors.grey.shade800,
                              fontSize: 2.5 * SIZE,
                              fontWeight: FontWeight.bold,
                            )),
                        Text('${getTimeFromNow(comment.createAt)}',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 2.5 * SIZE,
                              fontStyle: FontStyle.italic,
                            )),
                        SizedBox(
                          height: SIZE,
                        ),
                        Text(
                          comment.content,
                          style: TextStyle(fontSize: 2.5 * SIZE),
                        ),
                        Divider(),
                      ],
                    ),
                  ),
                ),
              ],
            );
          } else if (snapshot.hasError) {
            print('erreur lors du chargement de l\'auteur du commentaire');
          }
          return Container();
        });
  }
}
