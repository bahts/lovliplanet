import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/models/theme.dart' as model;
import 'package:flutter_app/screens/main/eventDetail.dart';
import 'package:flutter_app/screens/main/profil.dart';
import 'package:flutter_app/utils/date.dart';
import 'package:flutter_app/widgets/avatar.dart';
import 'package:flutter_app/widgets/cover.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';

class EventWidget extends StatelessWidget {
  final Event event;
  final User currentUser;
  final List<Event> events;
  final SocketIO eventSocket;
  final List<model.Theme> themes;

  const EventWidget(
      {@required this.event,
      @required this.currentUser,
      this.events,
      this.eventSocket,
      this.themes});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return EventWidgetFul(
      event: event,
      currentUser: currentUser,
      events: events,
      eventSocket: eventSocket,
      themes: themes,
    );
  }
}

class EventWidgetFul extends StatefulWidget {
  final Event event;
  final User currentUser;
  final List<Event> events;
  final SocketIO eventSocket;
  final List<model.Theme> themes;

  const EventWidgetFul(
      {@required this.event,
      @required this.currentUser,
      this.events,
      this.eventSocket,
      this.themes});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return EventWidgetState();
  }
}

class EventWidgetState extends State<EventWidgetFul> {
  Event _event;
  User _currentUser;
  List<Event> _events;
  SocketIO _eventSocket;
  List<model.Theme> _themes = [];

  @override
  void initState() {
    super.initState();
    _event = widget.event;
    _themes = widget.themes;
    _eventSocket = widget.eventSocket;
    _currentUser = widget.currentUser;
    _events = widget.events;
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return FutureBuilder<User>(
      future: User.getUser(_event.author),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final User _author = snapshot.data;
          return GestureDetector(
            onTap: () {
              _event.updateViewers(_currentUser.id).then((event) {
                setState(() {
                  _event.viewers = event.viewers;
                  if (!_event.viewers.contains(_currentUser.id))
                    _event.viewers.add(_currentUser.id);
                });

                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => EventDetail(
                            currentUser: _currentUser,
                            event: _event,
                            events: _events,
                            eventSocket: _eventSocket,
                            themes: _themes,
                          ),
                    ));
              });
            },
            child: Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: Colors.grey.shade200,
                ),
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(1.8 * SIZE),
                ),
                margin: EdgeInsets.only(left: SIZE, right: SIZE, bottom: SIZE),
                child: Container(
                    padding:
                        EdgeInsets.only(left: SIZE, right: SIZE, top: SIZE),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.only(right: SIZE),
                                child: Cover(
                                  cover: _event.cover,
                                  defaultCover:
                                      'assets/images/illustration.png',
                                ),
                              ),
                              flex: 1,
                            ),
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  GestureDetector(
                                    child: Row(
                                      children: <Widget>[
                                        Avatar(
                                            user: _author,
                                            avatarRadius: 2.5 * SIZE),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0.5 * SIZE, 0, 0, 0),
                                              child: Text('${_author.login}',
                                                  style: TextStyle(
                                                    color: Colors.grey.shade800,
                                                    fontSize: 2.5 * SIZE,
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0.5 * SIZE, 0, 0, 0),
                                              child: Text(
                                                  '${getTimeFromNow(_event.createAt)}',
                                                  style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 2 * SIZE,
                                                    fontStyle: FontStyle.italic,
                                                  )),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                    onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              Profil(
                                                themes: _themes,
                                                eventSocket: _eventSocket,
                                                currentUser: _currentUser,
                                                user: _author,
                                                events: _events,
                                              ),
                                        )),
                                  ),
                                  Divider(),
                                  Text('${_event.title}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 2.5 * SIZE)),
                                  SizedBox(
                                    height: SIZE,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.style,
                                        color: Colors.orange,
                                      ),
                                      SizedBox(
                                        height: SIZE / 5,
                                      ),
                                      Text(
                                        _themes
                                            .where((theme) =>
                                                theme.id == _event.theme)
                                            .first
                                            .name,
                                        style: TextStyle(fontSize: 2.5 * SIZE),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: SIZE,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.location_on,
                                        color: Colors.orange,
                                      ),
                                      SizedBox(
                                        height: SIZE / 5,
                                      ),
                                      Text(
                                        '${_event.location}',
                                        style: TextStyle(fontSize: 2.5 * SIZE),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: SIZE,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Flexible(
                                        flex: 2,
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.date_range,
                                              color: Colors.orange,
                                            ),
                                            SizedBox(
                                              height: SIZE / 5,
                                            ),
                                            Text(
                                              '${formatEventDate(DateTime.fromMillisecondsSinceEpoch(_event.date))}',
                                              style: TextStyle(
                                                  fontSize: 2.5 * SIZE),
                                            )
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.access_time,
                                              color: Colors.orange,
                                            ),
                                            SizedBox(
                                              height: SIZE / 5,
                                            ),
                                            Text(
                                              '${formatEventTime(TimeOfDay.fromDateTime(DateTime.fromMillisecondsSinceEpoch(_event.date)))}',
                                              style: TextStyle(
                                                  fontSize: 2.5 * SIZE),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: SIZE,
                                  ),
                                ],
                              ),
                              flex: 2,
                            )
                          ],
                        ),
                        Divider(),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('${_event.participants.length} participants',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 2.5 * SIZE,
                                  fontStyle: FontStyle.italic,
                                )),
                            Text('${_event.viewers.length} vues',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 2.5 * SIZE,
                                  fontStyle: FontStyle.italic,
                                )),
                            Text('${_event.comments.length} commentaires',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 2.5 * SIZE,
                                  fontStyle: FontStyle.italic,
                                )),
                          ],
                        ),
                        SizedBox(
                          height: SIZE,
                        )
                      ],
                    )),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          print("erreur dans le chargement de l'auteur");
        }
        return Container(
          height: 38 * SIZE,
          child: Card(
            margin: EdgeInsets.fromLTRB(SIZE / 5, SIZE, SIZE / 5, 0),
            color: Colors.grey.shade100,
          ),
        );
      },
    );
  }
}
