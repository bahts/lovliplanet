import 'package:flutter/material.dart';
import 'package:flutter_app/models/user.dart';
import 'dart:convert';

class Avatar extends StatelessWidget {
  final User user;
  final double avatarRadius;

  const Avatar({Key key, @required this.user, this.avatarRadius = 16.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return user.avatar.length == 0
        ? CircleAvatar(
            radius: avatarRadius,
            backgroundColor: Colors.orange.shade300,
            child: Text(
              '${user.login[0].toUpperCase()}',
              style: TextStyle(fontSize: avatarRadius),
            ),
          )
        : CircleAvatar(
            radius: avatarRadius,
            backgroundImage: MemoryImage(user.avatar),
          );
  }
}
