import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

import '../../models/user.dart';
import '../../utils/formsValidators.dart';

class ForgotScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Forgot();
  }
}

class Forgot extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ForgotState();
  }
}

class ForgotState extends State<Forgot> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final String _formatEmailValid = r'\w+\.\w+\d*\@orange\.com';
  bool _autoValidate = false;
  String _email;

  Widget FormUI(double size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextFormField(
          style: TextStyle(fontSize: 3 * size),
          onSaved: (String value) => _email = value,
          keyboardType: TextInputType.emailAddress,
          validator: validateEmail,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Colors.black38)),
              hasFloatingPlaceholder: true,
              labelText: 'Email',
              labelStyle: TextStyle(color: Colors.black38, fontSize: 3 * size)),
        ),
        SizedBox(
          height: 4.5 * size,
        ),
        CupertinoButton(
          child: Text(
            'Soumettre',
            style: TextStyle(fontSize: 3 * size),
          ),
          onPressed: _validateForm,
          color: Colors.orange,
          padding: EdgeInsets.fromLTRB(20 * size, size, 20 * size, size),
        ),
        SizedBox(
          height: 2 * size,
        ),
        FlatButton(
          child: Text("Se connecter",
              style: TextStyle(
                color: Colors.black26,
                fontSize: 2.5 * size,
                decoration: TextDecoration.underline,
              )),
          onPressed: () => Navigator.pushNamed(context, '/login'),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding:
            MediaQuery.of(context).size.height > 560 ? true : false,
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.fromLTRB(3.2 * SIZE, 9 * SIZE, 2 * SIZE, 0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: GestureDetector(
                    child: Icon(
                      Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                      size: 6 * SIZE,
                    ),
                    onTap: () => Navigator.pop(context),
                  ),
                ),
                SizedBox(
                  height: 7 * SIZE,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Récuperation',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 6 * SIZE),
                  ),
                ),
                SizedBox(
                  height: 20 * SIZE,
                ),
                Form(
                  child: FormUI(SIZE),
                  key: _formKey,
                  autovalidate: _autoValidate,
                ),
              ],
            ),
          ),
        ));
  }

  void _validateForm() {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      User.lostPassword(_email).then((_) => displaySnackBar(
          "Un lien vous a été envoyé pour réinitialiser votre mot de passe.",
          _scaffoldKey,
          isSuccess: true));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}
