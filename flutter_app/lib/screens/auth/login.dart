import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../utils/formsValidators.dart';
import 'dart:io' show Platform;
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/user.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Login();
  }
}

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<Login> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email;
  String _password;
  bool _isLoading = false;

  Widget FormUI(double size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextFormField(
          style: TextStyle(fontSize: 3 * size),
          initialValue: 'bah01194151@gmail.com',
          onSaved: (String value) => _email = value,
          keyboardType: TextInputType.emailAddress,
          validator: validateEmail,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Colors.black38)),
              hasFloatingPlaceholder: true,
              labelText: 'Email',
              labelStyle: TextStyle(color: Colors.black38, fontSize: 3 * size)),
        ),
        SizedBox(
          height: 2 * size,
        ),
        TextFormField(
          style: TextStyle(fontSize: 3 * size),
          initialValue: 'Thierno2',
          onSaved: (String value) => _password = value,
          validator: (value) =>
              notEmptyValidator(value, "Veuillez saisir votre mot de passe"),
          obscureText: true,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Colors.black38)),
              hasFloatingPlaceholder: true,
              labelText: 'Mot de passe',
              labelStyle: TextStyle(color: Colors.black38, fontSize: 3 * size)),
        ),
        SizedBox(
          height: 4 * size,
        ),
        CupertinoButton(
          child: _isLoading
              ? CircularProgressIndicator(
                  backgroundColor: Colors.white,
                )
              : Text(
                  "Se connecter",
                  style: TextStyle(fontSize: 3 * size),
                ),
          onPressed: _validateForm,
          color: Colors.orange,
          padding: EdgeInsets.fromLTRB(20 * size, size, 20 * size, size),
        ),
        SizedBox(
          height: 2 * size,
        ),
        FlatButton(
          child: Text("Mot de passe oublié?",
              style: TextStyle(
                color: Colors.black26,
                fontSize: 2.4 * size,
                decoration: TextDecoration.underline,
              )),
          onPressed: () => Navigator.pushNamed(context, '/forgot'),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.fromLTRB(3 * SIZE, 9 * SIZE, 2 * SIZE, 0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: GestureDetector(
                    child: Icon(
                      Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                      size: 6 * SIZE,
                    ),
                    onTap: () => Navigator.pop(context),
                  ),
                ),
                SizedBox(
                  height: 7 * SIZE,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Connexion',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 6 * SIZE),
                  ),
                ),
                SizedBox(
                  height: 20 * SIZE,
                ),
                Form(
                  child: FormUI(SIZE),
                  key: _formKey,
                  autovalidate: _autoValidate,
                ),
              ],
            ),
          ),
        ));
  }

  void _validateForm() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      setState(() => _isLoading = true);
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      final response = await User.loginToLovli(_email, _password);
      if (response["successful"]) {
        final resp = await User.getUserByEmail(_email);
        if (resp.length != 0) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          User user = User.fromJson(resp[0]);
          user.llt_token = response["data"]["llt_token"];
          user.sendUpdateUser();
          prefs.setString('userId', user.id);
          Navigator.pushNamed(context, '/dashboard');
          return;
        }
      }
      setState(() => _isLoading = false);
      displaySnackBar("Email ou mot de passe incorrect", _scaffoldKey);
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}
