import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.only(top: 10 * SIZE, bottom: 3 * SIZE),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    'Lovli.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 5 * SIZE),
                  ),
                  Text(
                    'Planet.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 5 * SIZE,
                        color: Colors.orange),
                  ),
                ],
              ),
              SizedBox(
                height: SIZE,
              ),
              Text('Partageons nos savoirs',
                  style:
                      TextStyle(color: Colors.black26, fontSize: 2.5 * SIZE)),
              SizedBox(
                height: 3 * SIZE,
              ),
              Expanded(
                child: Image.asset('assets/images/illustration.png'),
              ),
              SizedBox(
                height: 5 * SIZE,
              ),
              CupertinoButton(
                child: Text(
                  'Se connecter',
                  style: TextStyle(fontSize: 3 * SIZE),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/login');
                },
                color: Colors.orange,
                padding: EdgeInsets.fromLTRB(20 * SIZE, SIZE, 20 * SIZE, SIZE),
              ),
              SizedBox(
                height: 2 * SIZE,
              ),
              CupertinoButton(
                child: Text(
                  "S'inscrire",
                  style: TextStyle(fontSize: 3 * SIZE, color: Colors.black),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/signup');
                },
                color: Colors.grey.shade200,
                padding: EdgeInsets.fromLTRB(23 * SIZE, SIZE, 23 * SIZE, SIZE),
              ),
              SizedBox(
                height: 2 * SIZE,
              ),
              FlatButton(
                child: Text("À propos",
                    style:
                        TextStyle(color: Colors.black26, fontSize: 2.5 * SIZE)),
                onPressed: () {
                  return showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        // Retrieve the text the user has typed in using our
                        // TextEditingController
                        content: Text('À propos'),
                      );
                    },
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
