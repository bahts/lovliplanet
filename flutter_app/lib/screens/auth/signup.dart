import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../utils/formsValidators.dart';
import 'dart:io' show Platform;

import '../../models/user.dart';

class SignupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Signup();
  }
}

class Signup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignupState();
  }
}

class SignupState extends State<Signup> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email;
  String _login;
  String _password;
  String _confPassword;
  bool _isLoading = false;

  Widget FormUI(double size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextFormField(
          style: TextStyle(fontSize: 3 * size),
          onSaved: (String value) => _login = value,
          validator: (value) =>
              notEmptyValidator(value, "Veuillez saisir votre pseudo"),
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Colors.black38)),
              hasFloatingPlaceholder: true,
              labelText: 'Login',
              labelStyle:
                  TextStyle(color: Colors.black38, fontSize: 3.1 * size)),
        ),
        SizedBox(
          height: 2 * size,
        ),
        TextFormField(
          style: TextStyle(fontSize: 3 * size),
          onSaved: (String value) => _email = value,
          keyboardType: TextInputType.emailAddress,
          validator: validateEmail,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Colors.black38)),
              hasFloatingPlaceholder: true,
              labelText: 'Email',
              labelStyle:
                  TextStyle(color: Colors.black38, fontSize: 3.1 * size)),
        ),
        SizedBox(
          height: 2 * size,
        ),
        TextFormField(
          style: TextStyle(fontSize: 3 * size),
          onSaved: (String value) => _password = value,
          validator: (value) =>
              notEmptyValidator(value, "Veuillez saisir votre mot de passe"),
          obscureText: true,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Colors.black38)),
              hasFloatingPlaceholder: true,
              labelText: 'Mot de passe',
              labelStyle:
                  TextStyle(color: Colors.black38, fontSize: 3.1 * size)),
        ),
        SizedBox(
          height: 2 * size,
        ),
        TextFormField(
          style: TextStyle(fontSize: 3 * size),
          onSaved: (String value) => _confPassword = value,
          validator: (value) =>
              notEmptyValidator(value, "Veuillez confirmer le mot de passe"),
          obscureText: true,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: Colors.black38)),
              hasFloatingPlaceholder: true,
              labelText: 'Confirmation',
              labelStyle:
                  TextStyle(color: Colors.black38, fontSize: 3.1 * size)),
        ),
        SizedBox(
          height: 5 * size,
        ),
        CupertinoButton(
          child: _isLoading
              ? CircularProgressIndicator(
                  backgroundColor: Colors.white,
                )
              : Text(
                  "S'inscrire",
                  style: TextStyle(fontSize: 3 * size),
                ),
          onPressed: _validateForm,
          color: Colors.orange,
          padding: EdgeInsets.fromLTRB(20 * size, 0, 20 * size, 0),
        ),
        SizedBox(
          height: 2 * size,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding:
            MediaQuery.of(context).size.height > 560 ? true : false,
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.fromLTRB(3 * SIZE, 9 * SIZE, 2 * SIZE, 0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: GestureDetector(
                    child: Icon(
                      Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                      size: 6 * SIZE,
                    ),
                    onTap: () => Navigator.pop(context),
                  ),
                ),
                SizedBox(
                  height: 6 * SIZE,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Inscription',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 6 * SIZE),
                  ),
                ),
                SizedBox(
                  height: 15 * SIZE,
                ),
                Form(
                  child: FormUI(SIZE),
                  key: _formKey,
                  autovalidate: _autoValidate,
                ),
              ],
            ),
          ),
        ));
  }

  void _validateForm() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      setState(() => _isLoading = true);
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      if (_confPassword == _password) {
        final response = await User.addUserToLovli(_email, _login, _password);
        setState(() => _isLoading = false);
        displaySnackBar(response["message"], _scaffoldKey,
            isSuccess: response["successful"], duration: 5);
        if (response["successful"]) {
          User(
            email: _email,
            login: _login,
            isAdmin: false,
          ).createUser();
        }
        return;
      } else {
        displaySnackBar(
            "Vous avez saisi deux differents mots de passe", _scaffoldKey);
      }
    }
    setState(() {
      _autoValidate = true;
    });
  }
}
