import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:io' show Platform;
import 'package:flutter_app/models/comment.dart';
import 'package:flutter_app/screens/main/handleEvent.dart';
import 'package:flutter_app/utils/date.dart';
import 'package:flutter_app/utils/formsValidators.dart';
import 'package:flutter_app/widgets/avatar.dart';
import 'package:flutter_app/widgets/comment.dart';
import 'package:flutter_app/widgets/cover.dart';
import '../../models/event.dart';
import 'package:flutter_app/models/theme.dart' as model;
import './displayUsers.dart';
import 'package:device_calendar/device_calendar.dart' as device;

import '../../models/user.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';

class EventDetail extends StatefulWidget {
  final User currentUser;
  final Event event;
  final List<Event> events;
  final SocketIO eventSocket;
  final List<model.Theme> themes;

  const EventDetail(
      {Key key,
      this.currentUser,
      this.event,
      this.events,
      this.eventSocket,
      this.themes})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return EventDetailState();
  }
}

class EventDetailState extends State<EventDetail> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController controller = TextEditingController();
  device.DeviceCalendarPlugin _deviceCalendarPlugin;
  List<device.Calendar> _calendars;

  Event _event;
  List<Event> _events;
  User _currentUser;
  String _comment;
  bool _autoValidate;
  bool _participle;
  SocketIO _eventSocket;
  List<model.Theme> _themes;

  EventDetailState() {
    _deviceCalendarPlugin = new device.DeviceCalendarPlugin();
  }

  Future _retrieveCalendars() async {
    //Retrieve user's calendars from mobile device
    //Request permissions first if they haven't been granted
    try {
      var permissionsGranted = await _deviceCalendarPlugin.hasPermissions();
      if (permissionsGranted.isSuccess && !permissionsGranted.data) {
        permissionsGranted = await _deviceCalendarPlugin.requestPermissions();
        print(permissionsGranted.data);
        if (!permissionsGranted.isSuccess || !permissionsGranted.data) {
          return;
        }
      }

      final calendarsResult = await _deviceCalendarPlugin.retrieveCalendars();
      setState(() {
        _calendars = calendarsResult.data;
      });
      _calendars
          .forEach((cal) => print("${cal.id}  ${cal.name}  ${cal.isReadOnly}"));
    } catch (err) {
      print(err);
    }
  }

  Future _addEventsToCalendar() async {
    //Method to add events to the user's calendar
    //If called, the list of mmaEvents will be iterated through and the mma
    // Events will be added to the user's selected calendar

    //If the events have previously been added by the user, they will have a
    // shared preference key for the Event ID and the event will be UPDATED
    // instead of CREATED

    //If events are successfully created/added, then the events that were
    // CREATED/UPDATED will be displayed to the user in the status string

    for (var calendar in _calendars) {
      print(calendar.name);
      //Before adding MMA Event to calendar, check if it is ready for calendar
      // (i.e. ensure it is properly formatted)
      if (!calendar.isReadOnly) {
        final eventTime = DateTime.fromMillisecondsSinceEpoch(_event.date);
        final eventToCreate = device.Event(calendar.id);
        eventToCreate.title = _event.title;
        eventToCreate.start = eventTime;
        eventToCreate.description = _event.description;
        String calendarEventId;
        if (calendarEventId != null) {
          eventToCreate.eventId = calendarEventId;
        }
        eventToCreate.end = eventTime.add(new Duration(hours: 3));
        final createEventResult =
            await _deviceCalendarPlugin.createOrUpdateEvent(eventToCreate);
        if (createEventResult.isSuccess &&
            (createEventResult.data?.isNotEmpty ?? false)) {
          print("Évènement ajouté avec succès");
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _event = widget.event;
    _themes = widget.themes;
    _eventSocket = widget.eventSocket;
    _events = widget.events;
    _currentUser = widget.currentUser;
    _autoValidate = false;
    _participle = _event.participants.contains(_currentUser.id);
    _retrieveCalendars();
  }

  void _deleteEvent() {
    _event.deleteEvent().then((success) {
      if (success) {
        _eventSocket.emit('delete', [_event.id]);
        setState(() {
          _events.remove(_event);
        });
        displaySnackBar('Evènement supprimé avec succès', _scaffoldKey,
            isSuccess: true);
        Navigator.pop(context);
      } else {
        displaySnackBar('Probème de connexion', _scaffoldKey, isSuccess: false);
      }
    });
    _scaffoldKey.currentState.removeCurrentSnackBar();
  }

  Widget _more(double size) {
    return Container(
      height: 18 * size,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (_) => HandleEvent(
                        currentUser: _currentUser,
                        events: _events,
                        event: _event,
                        scaffoldKey: _scaffoldKey,
                        eventSocket: _eventSocket,
                        themes: _themes,
                      ));
              _scaffoldKey.currentState.removeCurrentSnackBar();
            },
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.edit,
                  color: Colors.black,
                  size: 4 * size,
                ),
                SizedBox(
                  width: 4 * size,
                ),
                Text(
                  'Modifier',
                  style: TextStyle(fontSize: 3 * size, color: Colors.black),
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: _deleteEvent,
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.delete_outline,
                  color: Colors.black,
                  size: 4 * size,
                ),
                SizedBox(
                  width: 4 * size,
                ),
                Text(
                  'Supprimer',
                  style: TextStyle(fontSize: 3 * size, color: Colors.black),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          GestureDetector(
            onTap: () {
              _scaffoldKey.currentState.removeCurrentSnackBar();
            },
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.close,
                  color: Colors.black,
                  size: 4 * size,
                ),
                SizedBox(
                  width: 4 * size,
                ),
                Text(
                  'Annuler',
                  style: TextStyle(fontSize: 3 * size, color: Colors.black),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //_retrieveCalendars();
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Scaffold(
        key: _scaffoldKey,
        floatingActionButton: RaisedButton(
          padding: _participle == true
              ? EdgeInsets.fromLTRB(15 * SIZE, SIZE, 15 * SIZE, SIZE)
              : EdgeInsets.fromLTRB(20 * SIZE, SIZE, 20 * SIZE, SIZE),
          elevation: 1.5 * SIZE,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(3.5 * SIZE),
          ),
          color: _participle == true
              ? Colors.orange.shade400
              : Colors.grey.shade300,
          child: Text(
            _participle == true ? 'Je ne participe plus' : 'Je participe',
            style: TextStyle(
                fontSize: 3 * SIZE,
                color: _participle == true ? Colors.white : Colors.black),
          ),
          onPressed: _handlePartipateButton,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        appBar: AppBar(
          actions: <Widget>[
            Chip(
              backgroundColor: Colors.grey.shade300,
              label: Text(
                _themes.where((theme) => _event.theme == theme.id).first.name,
                style: SIZE > 6 ? TextStyle(fontSize: 2 * SIZE) : TextStyle(),
              ),
            ),
            SizedBox(
              width: SIZE,
            ),
            (_currentUser.isAdmin && _currentUser.id == _event.author)
                ? IconButton(
                    onPressed: () {
                      final snackBar = SnackBar(
                          duration: Duration(days: 1),
                          backgroundColor: Colors.grey.shade100,
                          content: _more(SIZE));
                      _scaffoldKey.currentState.showSnackBar(snackBar);
                    },
                    icon: Icon(Icons.more_horiz),
                    iconSize: 4 * SIZE,
                  )
                : Container()
          ],
          title: FutureBuilder(
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                final User _author = snapshot.data;
                return Row(
                  children: <Widget>[
                    Avatar(
                      user: _author,
                      avatarRadius: 2.5 * SIZE,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text('${_author.login}',
                            style: TextStyle(
                              color: Colors.grey.shade800,
                              fontSize: 2.5 * SIZE,
                              fontWeight: FontWeight.bold,
                            )),
                        Text('${getTimeFromNow(_event.createAt)}',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 2 * SIZE,
                              fontStyle: FontStyle.italic,
                            )),
                      ],
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                print("erreur affichage des infos de l'auteur");
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            },
            future: User.getUser(_event.author),
          ),
          centerTitle: false,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          leading: GestureDetector(
            child: Icon(
              Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          padding: EdgeInsets.fromLTRB(1.5 * SIZE, 0, 1.5 * SIZE, 0),
          margin: EdgeInsets.symmetric(horizontal: SIZE),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 2 * SIZE),
                    child: Text(
                      _event.title,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 3.6 * SIZE),
                    )),
                SizedBox(
                  height: 1.4 * SIZE,
                ),
                Cover(
                  cover: _event.cover,
                  defaultCover: 'assets/images/illustration.png',
                ),
                SizedBox(height: 2 * SIZE),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.orange,
                          size: 4 * SIZE,
                        ),
                        SizedBox(
                          height: 1,
                        ),
                        Text(
                          _event.location,
                          style: TextStyle(fontSize: 3 * SIZE),
                        )
                      ],
                    ),
                    SizedBox(
                      height: SIZE,
                    ),
                    Row(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              color: Colors.orange,
                              size: 4 * SIZE,
                            ),
                            SizedBox(
                              height: SIZE / 5,
                            ),
                            Text(
                              '${formatEventDate(DateTime.fromMillisecondsSinceEpoch(_event.date))}',
                              style: TextStyle(fontSize: 3 * SIZE),
                            )
                          ],
                        ),
                        SizedBox(
                          width: 2 * SIZE,
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.access_time,
                              color: Colors.orange,
                              size: 4 * SIZE,
                            ),
                            SizedBox(
                              height: SIZE / 5,
                            ),
                            Text(
                              '${formatEventTime(TimeOfDay.fromDateTime(DateTime.fromMillisecondsSinceEpoch(_event.date)))}',
                              style: TextStyle(fontSize: 2.5 * SIZE),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => showDialog(
                          context: context,
                          builder: (_) => DisplayUsers(
                                users: _event.participants
                                    .map((userId) => User.getUser(userId))
                                    .toList(),
                                events: _events,
                                currentUser: _currentUser,
                              )),
                      child: Text('${_event.participants.length} participants',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 2.3 * SIZE,
                            fontStyle: FontStyle.italic,
                          )),
                    ),
                    GestureDetector(
                      onTap: () => showDialog(
                          context: context,
                          builder: (_) => DisplayUsers(
                                users: _event.viewers
                                    .toSet()
                                    .map((userId) => User.getUser(userId))
                                    .toList(),
                                events: _events,
                                currentUser: _currentUser,
                              )),
                      child: Text('${_event.viewers.length} vues',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 2.3 * SIZE,
                            fontStyle: FontStyle.italic,
                          )),
                    ),
                    Text('${_event.comments.length} commentaires',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 2.3 * SIZE,
                          fontStyle: FontStyle.italic,
                        )),
                  ],
                ),
                Divider(),
                SizedBox(
                  height: SIZE,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    _event.description,
                    style: TextStyle(fontSize: 2.5 * SIZE),
                  ),
                ),
                Divider(),
                SizedBox(
                  height: 1 * SIZE,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Commentaires   ${_event.comments.length}',
                      style: TextStyle(fontSize: 2.5 * SIZE),
                    ),
                  ],
                ),
                SizedBox(
                  height: 1.4 * SIZE,
                ),
                Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: Avatar(
                              user: _currentUser, avatarRadius: 2.5 * SIZE),
                          flex: 1,
                        ),
                        Flexible(
                          flex: 8,
                          child: TextFormField(
                            style: TextStyle(fontSize: 3 * SIZE),
                            controller: controller,
                            onSaved: (String value) => _comment = value,
                            validator: _validateField,
                            decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 0, color: Colors.black38)),
                                hasFloatingPlaceholder: true,
                                labelText: 'Commentaire...',
                                labelStyle: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 3.1 * SIZE)),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: IconButton(
                            icon: Icon(
                              Icons.send,
                              color: Colors.orange,
                              size: 5 * SIZE,
                            ),
                            onPressed: _validateFormComment,
                          ),
                        )
                      ],
                    )),
                SizedBox(
                  height: SIZE,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 3 * SIZE),
                  child: _event.comments.isNotEmpty
                      ? Column(
                          children: List<Widget>.from(
                              _event.comments.map((comment) => CommentWidget(
                                    comment: comment,
                                  ))).reversed.toList(),
                        )
                      : Text('Aucun commentaire',
                          style: TextStyle(fontSize: 3 * SIZE)),
                ),
                SizedBox(
                  height: 6 * SIZE,
                ),
              ],
            ),
          ),
        ));
  }

  String _validateField(String value) {
    if (value.length > 0) {
      return null;
    } else {
      return 'Vous ne pouvez pas soumettre un commentaire vide';
    }
  }

  void _validateFormComment() {
    FocusScope.of(context)
        .requestFocus(new FocusNode()); // faire disparaitre le clavier
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      // Ajout d'un commentaire
      Comment(author: _currentUser.id, content: _comment, event: _event.id)
          .addComment(_event.id)
          .then((event) {
        setState(() {
          _event.comments = event.comments;
        });
        controller.text = "";
        _eventSocket
            .emit('comment', [event.toJson(event.id)]); // vider le champ
      }).catchError((err) => print(err));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void _handlePartipateButton() {
    _event.updateParticipants(_currentUser.id, !_participle).then((event) {
      setState(() {
        _event.participants = event.participants;
        _participle = !_participle;
      });
    });
  }
}
