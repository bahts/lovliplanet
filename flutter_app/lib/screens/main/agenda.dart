import 'package:date_utils/date_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/cover.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/utils/date.dart';
import 'package:flutter_app/models/theme.dart' as model;
import 'package:adhara_socket_io/adhara_socket_io.dart';
import './eventDetail.dart';

class Agenda extends StatefulWidget {
  Agenda(
      {Key key, this.currentUser, this.events, this.eventSocket, this.themes})
      : super(key: key);

  final User currentUser;
  final List<Event> events;
  final SocketIO eventSocket;
  final List<model.Theme> themes;

  @override
  _AgendaState createState() => _AgendaState();
}

class _AgendaState extends State<Agenda> with TickerProviderStateMixin {
  DateTime _selectedDay;
  Map<DateTime, List<Event>> _events;
  Map<DateTime, List<Event>> _holidays;
  Map<DateTime, List<Event>> _visibleEvents;
  Map<DateTime, List<Event>> _visibleHolidays;
  List<Event> _selectedEvents;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    final partEvents = widget.events
        .where((event) => event.participants.contains(widget.currentUser.id));
    _selectedDay = DateTime.now();
    _holidays = Map<DateTime, List<Event>>();
    _events = Map<DateTime, List<Event>>();
    partEvents
        .map((event) =>
            formatEventDate(DateTime.fromMillisecondsSinceEpoch(event.date)))
        .toSet()
        .forEach((dateString) {
      final events = partEvents
          .where((event) =>
              formatEventDate(
                  DateTime.fromMillisecondsSinceEpoch(event.date)) ==
              dateString)
          .toList();

      final date =
          dateString.split('/').map((value) => num.parse(value)).toList();
      _events.putIfAbsent(DateTime(date[2], date[1], date[0]), () => events);
    });
    _selectedEvents = _events[_selectedDay] ?? [];
    _visibleEvents = _events;
    _visibleHolidays = _holidays;

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _controller.forward();
  }

  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      _selectedEvents = List<Event>.from(events);
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    setState(() {
      _visibleEvents = Map.fromEntries(
        _events.entries.where(
          (entry) =>
              entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );

      _visibleHolidays = Map.fromEntries(
        _holidays.entries.where(
          (entry) =>
              entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Scaffold(
      /*appBar: AppBar(
        title: Text(widget.title),
      ),*/
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          // Switch out 2 lines below to play with TableCalendar's settings
          //-----------------------
          _buildTableCalendarWithBuilders(SIZE),
          // _buildTableCalendarWithBuilders(),
          const SizedBox(height: 8.0),
          Expanded(child: _buildEventList(SIZE)),
        ],
      ),
    );
  }

  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders(double size) {
    return TableCalendar(
      locale: 'fr_FR',
      events: _visibleEvents,
      holidays: _visibleHolidays,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: 'Mois',
        CalendarFormat.twoWeeks: '2 semaines',
        CalendarFormat.week: 'Semaine',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color: Colors.blue[800]),
        holidayStyle: TextStyle().copyWith(color: Colors.blue[800]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: true,
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_controller),
            child: Container(
              margin: EdgeInsets.all(size),
              padding: EdgeInsets.only(top: size, left: size),
              color: Colors.orange.shade400,
              width: 20 * size,
              height: 20 * size,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 3 * size),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: EdgeInsets.all(size),
            padding: EdgeInsets.only(top: size, left: size),
            color: Colors.amber[400],
            width: 20 * size,
            height: 20 * size,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 3 * size),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, _visibleEvents[date], size),
              ),
            );
          }

          if (holidays.isNotEmpty) {
            children.add(
              Positioned(
                right: -2,
                top: -2,
                child: _buildHolidaysMarker(),
              ),
            );
          }

          return children;
        },
      ),
      onDaySelected: (date, events) {
        _onDaySelected(date, events ?? []);
        _controller.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  Widget _buildEventsMarker(DateTime date, List<Event> events, double size) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Utils.isSameDay(date, _selectedDay)
            ? Colors.brown[200]
            : Utils.isSameDay(date, DateTime.now())
                ? Colors.brown[500]
                : Colors.orange,
      ),
      width: 3 * size,
      height: 3 * size,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 2.5 * size,
          ),
        ),
      ),
    );
  }

  Widget _buildHolidaysMarker() {
    return Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.blueGrey[800],
    );
  }

  Widget _buildEventList(double size) {
    return ListView(
      children: _selectedEvents
          .map((event) => Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.shade100,
                  ),
                ],
              ),
              margin: EdgeInsets.zero,
              child: Card(
                color: Colors.white,
                child: ListTile(
                  trailing: Icon(Icons.keyboard_arrow_right),
                  leading: Cover(
                    cover: event.cover,
                    defaultCover: 'assets/images/illustration.png',
                  ),
                  title: Text(event.title),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(formatEventTime(TimeOfDay.fromDateTime(
                          DateTime.fromMillisecondsSinceEpoch(event.date)))),
                      Text(' • '),
                      Text(event.location)
                    ],
                  ),
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => EventDetail(
                              currentUser: widget.currentUser,
                              event: event,
                              events: widget.events,
                              eventSocket: widget.eventSocket,
                              themes: widget.themes,
                            ),
                      )),
                ),
              )))
          .toList(),
    );
  }
}
