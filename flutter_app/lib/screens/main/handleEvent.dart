import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/utils/date.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import '../../utils/formsValidators.dart';
import '../../models/theme.dart' as model;
import 'package:flutter_app/widgets/cover.dart';

class HandleEvent extends StatefulWidget {
  final User currentUser;
  final List<Event> events;
  final List<model.Theme> themes;
  final Event event; // est inserer s'il doit être modifié
  final scaffoldKey;
  final SocketIO eventSocket;
  final Function(String, String, String) showNotificationWithSound;

  const HandleEvent(
      {Key key,
      this.currentUser,
      this.scaffoldKey,
      this.events,
      this.event,
      this.showNotificationWithSound,
      this.eventSocket,
      this.themes})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HandleEventState();
  }
}

class HandleEventState extends State<HandleEvent>
    with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  User _currentUser;
  SocketIO _eventSocket;
  bool _autoValidate = false;
  String _title;
  String _description;
  String _location;
  String _theme;
  bool _isThemeCorrect = true;
  Event _event; // savoir si on modifie un évènemnt
  DateTime _date;
  TimeOfDay _time;
  AnimationController controller;
  Animation<double> scaleAnimation;
  List<Event> _events = [];
  List<model.Theme> _themes = [];
  List<int> _cover;
  bool _pickedImage = false;

  @override
  void initState() {
    super.initState();
    _event = widget.event;
    _cover = _event != null ? _event.cover : null;
    _eventSocket = widget.eventSocket;
    _date = _event == null
        ? DateTime.now()
        : DateTime.fromMillisecondsSinceEpoch(_event.date);
    _time = _event == null
        ? TimeOfDay.now()
        : TimeOfDay.fromDateTime(
            DateTime.fromMillisecondsSinceEpoch(_event.date));
    _title = _event == null ? '' : _event.title;
    _description = _event == null ? '' : _event.description;
    _location = _event == null ? '' : _event.location;
    _theme = _event == null ? '' : _event.theme;
    _events = widget.events;
    _themes = widget.themes;
    _currentUser = widget.currentUser;
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  Future _pickImage(ImageSource imageSource) async {
    Navigator.pop(context);
    final File cover = await ImagePicker.pickImage(
        source: imageSource, maxWidth: 256, maxHeight: 256);
    if (cover != null) {
      setState(() {
        _cover = cover.readAsBytesSync();
        _pickedImage = true;
      });
    }
  }

  void _displayImagePickerChoices() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[
                  GestureDetector(
                    child: new Text("Ouvrir l'appareil photo"),
                    onTap: () => _pickImage(ImageSource.camera),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    child: new Text('Ouvrir la galérie'),
                    onTap: () => _pickImage(ImageSource.gallery),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future _selectDate(BuildContext context) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    DateTime pickedDate = await showDatePicker(
        locale: const Locale('fr', 'FR'),
        context: context,
        initialDate: _date,
        firstDate: DateTime(2019),
        lastDate: DateTime(2025));

    if (pickedDate != null) setState(() => _date = pickedDate);
  }

  Future _selectTime(BuildContext context) async {
    TimeOfDay pickedTime = await showTimePicker(
      context: context,
      initialTime: _time,
    );

    if (pickedTime != null) setState(() => _time = pickedTime);
  }

  Widget formUI(double size) {
    return SingleChildScrollView(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              initialValue: _title,
              validator: _validateField,
              style: TextStyle(fontSize: 3 * size),
              onSaved: (String value) => _title = value,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  icon: Icon(
                    Icons.keyboard,
                    color: Colors.orange,
                    size: 4 * size,
                  ),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.black38)),
                  hasFloatingPlaceholder: true,
                  labelText: 'Titre',
                  labelStyle:
                      TextStyle(color: Colors.black38, fontSize: 3.5 * size)),
            ),
            SizedBox(
              height: 6 * size,
            ),
            TextFormField(
              initialValue: _description,
              validator: _validateField,
              style: TextStyle(fontSize: 3 * size),
              onSaved: (String value) => _description = value,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                  icon: Icon(
                    Icons.comment,
                    color: Colors.orange,
                    size: 4 * size,
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.black38)),
                  hasFloatingPlaceholder: true,
                  labelText: 'Description',
                  labelStyle:
                      TextStyle(color: Colors.black38, fontSize: 3.5 * size)),
            ),
            SizedBox(
              height: 2 * size,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Icon(
                    Icons.style,
                    color: Colors.orange,
                    size: 4 * size,
                  ),
                ),
                Flexible(
                    flex: 8,
                    child: DropdownButton<String>(
                      iconSize: 5 * size,
                      hint: _theme.isEmpty
                          ? Text("Thème")
                          : Text(
                              _themes
                                  .firstWhere((theme) => theme.id == _theme)
                                  .name,
                              style: TextStyle(color: Colors.black),
                            ),
                      isExpanded: true,
                      items: _themes.map((model.Theme theme) {
                        return DropdownMenuItem<String>(
                          value: theme.id,
                          child: Text(theme.name),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _theme = value;
                        });
                      },
                    ))
              ],
            ),
            SizedBox(
              height: size,
            ),
            TextFormField(
              initialValue: _location,
              validator: _validateField,
              style: TextStyle(fontSize: 3 * size),
              onSaved: (String value) => _location = value,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  icon: Icon(
                    Icons.location_on,
                    color: Colors.orange,
                    size: 4 * size,
                  ),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.black38)),
                  hasFloatingPlaceholder: true,
                  labelText: 'Lieu',
                  labelStyle:
                      TextStyle(color: Colors.black38, fontSize: 3.5 * size)),
            ),
            SizedBox(
              height: size,
            ),
            Row(
              children: <Widget>[
                Flexible(
                  child: Theme(
                    data: Theme.of(context).copyWith(
                        primaryColor: Colors.orange //color of the main banner
                        ),
                    child: Builder(
                        builder: (BuildContext context) => GestureDetector(
                              onTap: () => _selectDate(context),
                              behavior: HitTestBehavior.opaque,
                              child: TextFormField(
                                enabled: _selectDate == null,
                                decoration: InputDecoration(
                                    icon: Icon(
                                      Icons.date_range,
                                      color: Colors.orange,
                                      size: 4 * size,
                                    ),
                                    labelText: '${formatEventDate(_date)}',
                                    labelStyle: TextStyle(
                                        color: Colors.black,
                                        fontSize: 3.1 * size)),
                              ),
                            )),
                  ),
                ),
                Flexible(
                  child: Theme(
                    data: Theme.of(context).copyWith(
                        primaryColor: Colors.orange //color of the main banner
                        ),
                    child: Builder(
                        builder: (BuildContext context) => GestureDetector(
                              onTap: () => _selectTime(context),
                              behavior: HitTestBehavior.opaque,
                              child: TextFormField(
                                enabled: _selectDate == null,
                                decoration: InputDecoration(
                                    icon: Icon(
                                      Icons.date_range,
                                      color: Colors.orange,
                                      size: 4 * size,
                                    ),
                                    labelText: '${formatEventTime(_time)}',
                                    labelStyle: TextStyle(
                                        color: Colors.black,
                                        fontSize: 3.1 * size)),
                              ),
                            )),
                  ),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.only(left: 8 * size, top: size),
              alignment: Alignment.centerLeft,
              child: !_isThemeCorrect
                  ? Text('Veuillez selectionner un thème',
                      style: TextStyle(fontSize: 2.5 * size, color: Colors.red))
                  : null,
            ),
            SizedBox(
              height: 5 * size,
            ),
            _cover == null
                ? GestureDetector(
                    onTap: _displayImagePickerChoices,
                    child: Container(
                      width: 20 * size,
                      height: 20 * size,
                      child: Icon(
                        Icons.add_a_photo,
                        size: 20 * size,
                      ),
                    ),
                  )
                : Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Cover(
                        cover: _cover,
                        width: 40 * size,
                        defaultCover: 'assets/images/illustration.png',
                      ) /*Cover(
                    cover: event.cover,
                    defaultCover: 'assets/images/illustration.png',
                  )*/
                      ,
                      IconButton(
                        onPressed: _displayImagePickerChoices,
                        icon: Icon(Icons.edit),
                        color: Colors.black87,
                        iconSize: 7 * size,
                        alignment: Alignment.topLeft,
                      )
                    ],
                  ),
            SizedBox(
              height: 5 * size,
            ),
            CupertinoButton(
              color: Colors.orange,
              child: Text(
                _event == null ? "Publier" : 'Modifier',
                style: TextStyle(fontSize: 3 * size),
              ),
              onPressed: _validateForm,
              padding: EdgeInsets.fromLTRB(20 * size, size, 20 * size, size),
            )
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(4 * SIZE),
              padding: EdgeInsets.all(5 * SIZE),
              height: 120 * SIZE,
              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: formUI(SIZE),
              )),
        ),
      ),
    );
  }

  String _validateField(String text) {
    if (text.length > 0) {
      return null;
    }
    return 'Ce champ ne peut être vide';
  }

  void _validateForm() {
    FocusScope.of(context).requestFocus(new FocusNode());
    final dateTime =
        DateTime(_date.year, _date.month, _date.day, _time.hour, _time.minute);
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      if (_theme.length > 0) {
        _event == null ? _createEvent(dateTime) : _updateEvent(dateTime);
      } else {
        setState(() {
          _autoValidate = true;
          _isThemeCorrect = _theme.length > 0;
        });
      }
    }
  }

  void _createEvent(DateTime dateTime) {
    Event(
      author: _currentUser.id,
      date: dateTime.millisecondsSinceEpoch,
      description: _description,
      location: _location,
      theme: _theme,
      title: _title,
      cover: _cover,
    ).createEvent().then((event) {
      _eventSocket.emit('create', [json.encode(event.toJson(event.id))]);
      setState(() {
        _events.add(event);
      });
      Navigator.pop(context);
      displaySnackBar('Evènement créé avec succès', widget.scaffoldKey,
          isSuccess: true);
    }).catchError((err) {
      print(err);
      displaySnackBar('Problème de connexion internet', widget.scaffoldKey,
          isSuccess: false);
    });
  }

  void _updateEvent(DateTime dateTime) {
    setState(() {
      _event.title = _title;
      _event.description = _description;
      _event.theme = _theme;
      _event.date = dateTime.millisecondsSinceEpoch;
      _event.location = _location;
      _event.cover = _cover;
    });
    _event.sendUpdateEvent().then((editEvent) {
      print(editEvent.theme);
      final int index = _events.indexWhere((event) => event.id == editEvent.id);
      _eventSocket.emit('edit', [json.encode(editEvent.toJson(editEvent.id))]);
      setState(() {
        _events.replaceRange(index, index + 1, [editEvent]);
      });
      Navigator.pop(context);
      displaySnackBar('Évènement modifié avec succès', widget.scaffoldKey,
          isSuccess: true);
    }).catchError((err) {
      print(err);
      displaySnackBar('Problème de connexion', widget.scaffoldKey,
          isSuccess: false);
    });
  }
}
