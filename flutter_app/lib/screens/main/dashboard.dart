import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/socketClientAPI.dart';
import 'package:flutter_app/screens/main/displayEvents.dart';
import 'package:flutter_app/screens/main/eventDetail.dart';
import 'dart:async';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/notification.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:flutter_app/screens/main/handleEvent.dart';
import 'package:flutter_app/screens/main/profil.dart';
import 'package:flutter_app/widgets/avatar.dart';
import 'package:flutter_app/widgets/listEvent.dart';
import 'package:flutter_app/widgets/theme.dart';
import './handleTheme.dart';

import '../../models/user.dart';
import '../../models/theme.dart' as model;
import './agenda.dart';
import './search.dart';
import './notification.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dashboard();
  }
}

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DashboardState();
  }
}

class DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  User _user;
  List<Event> _events = [];
  List<model.Theme> _themes;
  bool _isOpened = false;
  bool _newNotif = false;

  int _currentPage = 0, _limit = 4;
  ScrollController _scrollController =
      ScrollController(initialScrollOffset: 0.0, keepScrollOffset: true);

  SocketIOManager _manager;
  SocketIO _eventSocket, _themeSocket;

  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  //constructor
  DashboardState() {
    _scrollController.addListener(() {
      var isEnd = _scrollController.offset ==
          _scrollController.position.maxScrollExtent;
      if (isEnd) {
        getEvents();
      }
    });
    getEvents();
  }

  Future initSockets() async {
    SocketIO eventSocket =
        await SocketClientAPI('/event', _manager).initSocket();
    SocketIO themeSocket =
        await SocketClientAPI('/theme', _manager).initSocket();

    setState(() {
      _eventSocket = eventSocket;
      _themeSocket = themeSocket;
    });

    // Handle theme socket events

    _themeSocket.on('new_theme', (data) async {
      model.Theme newTheme = model.Theme.fromJson(data);
      setState(() {
        _themes.add(newTheme);
        _newNotif = true;
      });
      NotificationObj(
              genre: 'theme',
              content:
                  "Le thème ${newTheme.name} vient d'être créé, abonnez vous pour ne rien manquer !",
              target: newTheme.id,
              viewed: false)
          .pushNotification(_user.id)
          .then((notifications) {
        setState(() {
          _user.notifications = notifications;
        });
      }).catchError((err) => print(err));

      if (_user.enableNotification) {
        await _showNotificationWithSound(
            'Nouveau thème',
            "Le thème ${newTheme.name} vient d'être créé, abonnez vous pour ne rien manquer !",
            "theme-${newTheme.id}");
      }
    });

    _themeSocket.on('edit_theme', (data) async {
      model.Theme editTheme = model.Theme.fromJson(data);
      int index = _themes.indexWhere((theme) => theme.id == editTheme.id);

      setState(() {
        _themes.replaceRange(index, index + 1, [editTheme]);
      });

      if (_user.enableNotification) {
        await _showNotificationWithSound('Évènement modifié',
            "Un thème vient d'être mis à jour", "theme-${editTheme.id}");
      }
    });

    _eventSocket.on('delete_theme', (data) async {
      setState(() {
        _themes.remove(_themes.firstWhere((theme) => theme.id == data));
      });
    });

    // Handle event socket events

    _eventSocket.on('new_event', (data) async {
      Event newEvent = Event.fromJson(data);
      setState(() {
        _events.add(newEvent);
        _newNotif = true;
      });
      final snackBar = SnackBar(
        duration: Duration(seconds: 10),
        content: Text('Nouveaux évènements !'),
        action: SnackBarAction(
          label: 'Actualiser',
          onPressed: () => _refreshIndicatorKey.currentState.show(),
        ),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);

      NotificationObj(
              genre: 'event',
              content:
                  "Un évènement a été publié dans la catégorie ${_themes.where((theme) => theme.id == newEvent.theme).first.name}, n'hesitez pas à participer",
              target: newEvent.id,
              viewed: false)
          .pushNotification(_user.id)
          .then((notifications) {
        setState(() {
          _user.notifications = notifications;
        });
      }).catchError((err) => print(err));

      if (_user.enableNotification) {
        await _showNotificationWithSound(
            'Nouvel évènement',
            "Un évènement a été publié dans la catégorie ${_themes.where((theme) => theme.id == newEvent.theme).first.name}, n'hesitez pas à participer",
            "event-${newEvent.id}");
      }
    });

    _eventSocket.on('edit_event', (data) async {
      Event editEvent = Event.fromJson(data);
      int index = _events.indexWhere((event) => event.id == editEvent.id);

      setState(() {
        _events.replaceRange(index, index + 1, [editEvent]);
      });

      if (_user.enableNotification) {
        await _showNotificationWithSound(
            'Évènement modifié',
            "Un évènement de la catégorie ${_themes.where((theme) => theme.id == editEvent.theme).first.name} vient d'être mis à jour",
            "event-${editEvent.id}");
      }
    });

    _eventSocket.on('new_comment', (data) async {
      Event editEvent = Event.fromJson(data);
      int index = _events.indexWhere((event) => event.id == editEvent.id);

      setState(() {
        _events.replaceRange(index, index + 1, [editEvent]);
      });
      /*
      if (_user.enableNotification) {
        await _showNotificationWithSound(
          'Commentaire publié',
          "Un commentaire vient d'être publié sur un évènement qui vous intéresse",
          "event-${editEvent.id}");
      }
      */
    });

    _eventSocket.on('delete_event', (data) async {
      setState(() {
        _events.remove(_events.firstWhere((event) => event.id == data));
      });
    });
  }

  Future getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString('userId') ?? "";
    if (userId == "") return;
    final user = await User.getUser(userId);
    if (!mounted) return;
    setState(() {
      _user = user;
    });
  }

  Future getEvents() async {
    final events = await Event.getEvents(_currentPage, _limit);
    if (!mounted) return;
    if (events.isNotEmpty) {
      setState(() {
        _events.addAll(events);
      });
      _currentPage += _limit;
    }
  }

  Future getThemes() async {
    final themes = await model.Theme.getThemes();
    if (!mounted) return;
    setState(() {
      _themes = themes;
    });
  }

  @override
  initState() {
    super.initState();

    _manager = SocketIOManager();
    /*
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show()); */

    initSockets();

    // mise ne place de la notification
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    // If you have skipped STEP 3 then change  app_icon to @mipmap/ic_launcher
    var initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = new IOSInitializationSettings();

    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    // recuperation des evenements et des themes de l'application
    getUser();
    //getEvents();
    getThemes();

    // animation pour le bouton ajouter
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500))
          ..addListener(() {
            setState(() {});
          });
    _buttonColor = ColorTween(
      begin: Colors.white,
      end: Colors.white,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));
    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!_isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    setState(() {
      _isOpened = !_isOpened;
    });
  }

  Widget theme(double size) {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        onPressed: () => showDialog(
            context: context,
            builder: (_) => HandleTheme(
                  currentUser: _user,
                  themes: _themes,
                  scaffoldKey: _scaffoldKey,
                  themeSocket: _themeSocket,
                )),
        tooltip: 'Thème',
        child: Icon(
          Icons.style,
          color: Colors.white,
          size: 30,
        ),
      ),
    );
  }

  Widget event(double size) {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        onPressed: () => showDialog(
            context: context,
            builder: (_) => HandleEvent(
                  currentUser: _user,
                  events: _events,
                  scaffoldKey: _scaffoldKey,
                  eventSocket: _eventSocket,
                  themes: _themes,
                )),
        tooltip: 'Evènement',
        child: Icon(
          Icons.event,
          color: Colors.white,
          size: 30,
        ),
      ),
    );
  }

  Widget toggle(double size) {
    return Container(
      child: FloatingActionButton(
        heroTag: null,
        backgroundColor: _buttonColor.value,
        onPressed: animate,
        tooltip: _isOpened ? 'Fermer' : 'Ouvrir',
        child: Icon(
          _isOpened ? Icons.close : Icons.add,
          size: _isOpened ? 35 : 45,
          color: Colors.orange,
        ),
      ),
    );
  }

/*
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _user = ModalRoute.of(context).settings.arguments;
  }
  */

  Widget _callPage(int indexPage, double size) {
    if (_events == null || _themes == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    _events
        .sort((event1, event2) => event2.createAt.compareTo(event1.createAt));
    switch (indexPage) {
      case 0:
        return RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: getEvents,
          child: _events.isEmpty
              ? Center(
                  child: Text(
                    'Aucun évènement publié',
                    style: TextStyle(fontSize: 4 * size),
                  ),
                )
              : ListEvent(
                  _events, _user, _eventSocket, _themes, _scrollController),
        );

      case 1:
        return Agenda(
          currentUser: _user,
          events: _events,
          eventSocket: _eventSocket,
          themes: _themes,
        );
      case 2:
        return Search(_events, _user, _themes, _themeSocket, _eventSocket);
      case 3:
        setState(() {
          _newNotif = false;
        });
        return NotificationScreen(
            user: _user, events: _events, themes: _themes);
      default:
        return Container();
    }
  }

  Widget _menu(double size) {
    if (_themes != null) {
      List<Widget> themesList = <Widget>[
        DrawerHeader(
          padding: EdgeInsets.symmetric(horizontal: 12 * size),
          child: GestureDetector(
            child: Avatar(
              user: _user,
              avatarRadius: 15 * size,
            ),
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Profil(
                        themes: _themes,
                        themeSocket: _themeSocket,
                        eventSocket: _eventSocket,
                        currentUser: _user,
                        user: _user,
                        events: _events,
                      ),
                )),
          ),
          decoration: BoxDecoration(color: Colors.grey.shade200),
        )
      ];

      themesList.add(Card(
        color: Colors.orange.shade100,
        child: ListTile(
          title: Text('Abonnements'),
        ),
      ));
      themesList.addAll(List<Widget>.from(_themes
          .where((theme) => theme.subscribers.contains(_user.id))
          .map((theme) => ThemeWidget(
              theme, _user, _events, _themeSocket, _eventSocket, _themes))));

      themesList.add(Card(
        color: Colors.orange.shade100,
        child: ListTile(
          title: Text('Autres'),
        ),
      ));

      themesList.addAll(List<Widget>.from(_themes
          .where((theme) => !theme.subscribers.contains(_user.id))
          .map((theme) => ThemeWidget(
              theme, _user, _events, _themeSocket, _eventSocket, _themes))));

      return Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: themesList.toList(),
        ),
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    if (_user == null) {
      return Container(
        color: Colors.white,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: _user.isAdmin
          ? Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Transform(
                    transform: Matrix4.translationValues(
                      0.0,
                      _translateButton.value * 2.0,
                      0.0,
                    ),
                    child: theme(SIZE),
                  ),
                  Transform(
                    transform: Matrix4.translationValues(
                      0.0,
                      _translateButton.value,
                      0.0,
                    ),
                    child: event(SIZE),
                  ),
                  toggle(SIZE),
                ],
              ),
            )
          : null,
      appBar: AppBar(
        actions: <Widget>[
          GestureDetector(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Avatar(
                user: _user,
                avatarRadius: 20,
              ),
            ),
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => Profil(
                        themes: _themes,
                        eventSocket: _eventSocket,
                        themeSocket: _themeSocket,
                        currentUser: _user,
                        user: _user,
                        events: _events,
                      ),
                )),
          ),
          SizedBox(
            width: 3 * SIZE,
          )
        ],
        title: Text('LovliPlanet', style: TextStyle(color: Colors.black87)),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: _callPage(_currentIndex, SIZE),
      drawer: _menu(SIZE),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 5 * SIZE,
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: Text('Accueil')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.calendar_today,
              ),
              title: Text('Agenda')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
              ),
              title: Text('Recherche')),
          BottomNavigationBarItem(
              icon: Stack(children: <Widget>[
                Icon(Icons.notifications),
                Positioned(
                  // draw a red marble
                  top: -1.0,
                  right: -1.5,
                  child: new Icon(Icons.brightness_1,
                      size: 2 * SIZE,
                      color: _newNotif ? Colors.redAccent : Colors.white),
                )
              ]),
              title: Text('Notifications')),
        ],
        currentIndex: _currentIndex,
        selectedItemColor: Colors.orange,
        unselectedItemColor: Colors.grey.shade400,
        showUnselectedLabels: false,
        showSelectedLabels: true,
        onTap: (indexPage) {
          setState(() {
            _currentIndex = indexPage;
          });
        },
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    final List<String> payloads = payload.split('-');
    if (payloads[0] == 'event') {
      final event = _events.firstWhere((event) => event.id == payloads[1]);
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => EventDetail(
                  themes: _themes,
                  currentUser: _user,
                  event: event,
                  events: _events,
                ),
          ));
    } else {
      // otherwise, theme
      final model.Theme theme =
          _themes.firstWhere((theme) => theme.id == payloads[1]);
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => DisplayEvents(
                  title: theme.name,
                  themes: [theme],
                  currentUser: _user,
                  events: List<Event>.from(
                      _events.where((event) => event.theme == theme.name)),
                ),
          ));
    }
  }

  // Methode 1
  Future _showNotificationWithSound(
      String titre, String desciption, String payload) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        sound: 'slow_spring_board',
        showProgress: true,
        color: Colors.orange,
        importance: Importance.Max,
        priority: Priority.High);

    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(sound: "slow_spring_board.mp3");

    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
      0,
      titre,
      desciption,
      platformChannelSpecifics,
      payload: payload,
    );
  }

  // Methode 2
  Future _showNotificationWithDefaultSound() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High);

    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();

    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
      0,
      'New Post',
      'How to Show Notification in Flutter',
      platformChannelSpecifics,
      payload: 'Default_Sound',
    );
  }

  // Methode 3
  Future _showNotificationWithoutSound() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        playSound: false, importance: Importance.Max, priority: Priority.High);

    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(presentSound: false);

    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
      0,
      'New Post',
      'How to Show Notification in Flutter',
      platformChannelSpecifics,
      payload: 'No_Sound',
    );
  }
}
