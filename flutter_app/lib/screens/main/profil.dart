import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/utils/formsValidators.dart';
import 'package:flutter_app/widgets/avatar.dart';
import '../main/displayEvents.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../../utils/contants.dart';
import '../../models/theme.dart' as model;
import 'package:adhara_socket_io/adhara_socket_io.dart';

class Profil extends StatefulWidget {
  final User currentUser;
  final User user;
  final List<Event> events;
  final List<model.Theme> themes;
  final SocketIO eventSocket, themeSocket;

  const Profil(
      {Key key,
      this.currentUser,
      this.user,
      this.events,
      this.themes,
      this.eventSocket,
      this.themeSocket})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProfilState();
  }
}

class ProfilState extends State<Profil> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  User _user;
  User _currentUser;
  List<Event> _eventsCreated;
  List<model.Theme> _themes;
  SocketIO _eventSocket, _themeSocket;

  bool _editing = false;
  bool _autoValidate = false;
  String _oldPassword = "";
  String _newPassword = "";

  @override
  void initState() {
    super.initState();
    _user = widget.user;
    _currentUser = widget.currentUser;
    _eventsCreated = List<Event>.from(
        widget.events.where((event) => event.author == _user.id));
    _themes = widget.themes;
    _eventSocket = widget.eventSocket;
    _themeSocket = widget.themeSocket;
  }

  Widget _more(double size) {
    return Container(
      height: _user.id == _currentUser.id ? 23 * size : 12 * size,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          _user.id == _currentUser.id
              ? GestureDetector(
                  onTap: () {
                    _scaffoldKey.currentState.removeCurrentSnackBar();
                    setState(() {
                      _editing = !_editing;
                    });
                    print(_editing);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.edit,
                        color: Colors.black,
                        size: 4 * size,
                      ),
                      SizedBox(
                        width: 4 * size,
                      ),
                      Text(
                        'Modifier',
                        style:
                            TextStyle(fontSize: 3 * size, color: Colors.black),
                      )
                    ],
                  ),
                )
              : Container(),
          SizedBox(
            height: _user.id == _currentUser.id ? size : 0,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => DisplayEvents(
                          theme: null,
                          themes: _themes,
                          eventSocket: _eventSocket,
                          themeSocket: _themeSocket,
                          currentUser: _currentUser,
                          events: _eventsCreated,
                          title: 'Publications',
                        ),
                  ));
            },
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.event,
                  color: Colors.black,
                  size: 4 * size,
                ),
                SizedBox(
                  width: 4 * size,
                ),
                Text(
                  'Publications (${_eventsCreated.length})',
                  style: TextStyle(fontSize: 3 * size, color: Colors.black),
                )
              ],
            ),
          ),
          SizedBox(
            height: _user.id == _currentUser.id ? size : 0,
          ),
          _user.id == _currentUser.id
              ? GestureDetector(
                  onTap: () async {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    prefs.remove('userId');
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/', (Route<dynamic> route) => false);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.power_settings_new,
                        color: Colors.black,
                        size: 4 * size,
                      ),
                      SizedBox(
                        width: 4 * size,
                      ),
                      Text(
                        'Se deconnecter',
                        style:
                            TextStyle(fontSize: 3 * size, color: Colors.black),
                      )
                    ],
                  ),
                )
              : Container(),
          Divider(
            color: Colors.black,
          ),
          GestureDetector(
            onTap: () {
              _scaffoldKey.currentState.removeCurrentSnackBar();
            },
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.close,
                  color: Colors.black,
                  size: 4 * size,
                ),
                SizedBox(
                  width: 4 * size,
                ),
                Text(
                  'Annuler',
                  style: TextStyle(fontSize: 3 * size, color: Colors.black),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Future _pickImage(ImageSource imageSource) async {
    Navigator.pop(context);
    final File avatar = await ImagePicker.pickImage(
        source: imageSource, maxWidth: 256, maxHeight: 256);
    if (avatar != null) {
      setState(() {
        _currentUser.avatar = avatar.readAsBytesSync();
      });
    }
  }

  void _displayImagePickerChoices() {
    if (_editing) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: new SingleChildScrollView(
                child: new ListBody(
                  children: <Widget>[
                    GestureDetector(
                      child: new Text("Ouvrir l'appareil photo"),
                      onTap: () => _pickImage(ImageSource.camera),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                    ),
                    GestureDetector(
                      child: new Text('Ouvrir la galérie'),
                      onTap: () => _pickImage(ImageSource.gallery),
                    ),
                  ],
                ),
              ),
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              color: _editing ? Colors.green : Colors.black,
              onPressed: () {
                print(_editing);
                if (_editing) {
                  _validateForm();
                } else {
                  final snackBar = SnackBar(
                      duration: Duration(days: 1),
                      backgroundColor: Colors.grey.shade100,
                      content: _more(SIZE));
                  _scaffoldKey.currentState.showSnackBar(snackBar);
                }
              },
              icon: Icon(_editing ? Icons.done_outline : Icons.more_horiz),
              iconSize: 4 * SIZE,
            )
          ],
          title: Text(
            'Profil',
            style: TextStyle(
              color: Colors.black87,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black87),
          leading: GestureDetector(
            child: Icon(
              Icons.close,
              size: 30,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
            padding: EdgeInsets.fromLTRB(1.5 * SIZE, SIZE, 1.5 * SIZE, 0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: _displayImagePickerChoices,
                      child: Stack(
                        children: <Widget>[
                          Opacity(
                            opacity: _editing ? .7 : 1,
                            child: Avatar(
                              user: _user,
                              avatarRadius: 20 * SIZE,
                            ),
                          ),
                          Positioned(
                            top: 80,
                            left: 80,
                            child: _editing
                                ? Icon(Icons.edit,
                                    size: 9 * SIZE, color: Colors.black)
                                : Container(),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Row(
                      children: <Widget>[
                        Flexible(
                          child: Icon(
                            Icons.person,
                            color: Colors.orange,
                            size: 5.6 * SIZE,
                          ),
                          flex: 1,
                        ),
                        SizedBox(
                          width: SIZE,
                        ),
                        Flexible(
                          child: Text('${_user.login}',
                              style: TextStyle(fontSize: 3 * SIZE)),
                          flex: 10,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 2 * SIZE,
                    ),
                    Row(
                      children: <Widget>[
                        Flexible(
                          child: Icon(
                            Icons.mail,
                            color: Colors.orange,
                            size: 5.6 * SIZE,
                          ),
                          flex: 1,
                        ),
                        SizedBox(
                          width: SIZE,
                        ),
                        Flexible(
                          child: Text(
                            '${_user.email}',
                            style: TextStyle(fontSize: 3 * SIZE),
                          ),
                          flex: 11,
                        )
                      ],
                    ),
                    SizedBox(
                      height: SIZE,
                    ),
                    _editing
                        ? Row(
                            children: <Widget>[
                              Flexible(
                                child: Icon(
                                  Icons.lock,
                                  color: Colors.orange,
                                  size: 5.6 * SIZE,
                                ),
                                flex: 1,
                              ),
                              SizedBox(
                                width: SIZE,
                              ),
                              Flexible(
                                child: TextFormField(
                                    obscureText: true,
                                    keyboardType: TextInputType.emailAddress,
                                    style: TextStyle(fontSize: 3 * SIZE),
                                    onSaved: (String value) =>
                                        _oldPassword = value,
                                    decoration: InputDecoration(
                                      labelText: 'Ancien mot de passe',
                                      labelStyle: TextStyle(
                                          color: Colors.black38,
                                          fontSize: 3 * SIZE),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1, color: Colors.black38)),
                                    )),
                                flex: 11,
                              )
                            ],
                          )
                        : Container(),
                    SizedBox(
                      height: _editing ? SIZE : 0,
                    ),
                    _editing
                        ? Row(
                            children: <Widget>[
                              Flexible(
                                child: Icon(
                                  Icons.lock,
                                  color: Colors.orange,
                                  size: 5.6 * SIZE,
                                ),
                                flex: 1,
                              ),
                              SizedBox(
                                width: SIZE,
                              ),
                              Flexible(
                                child: TextFormField(
                                    obscureText: true,
                                    keyboardType: TextInputType.emailAddress,
                                    style: TextStyle(fontSize: 3 * SIZE),
                                    onSaved: (String value) =>
                                        _newPassword = value,
                                    decoration: InputDecoration(
                                      labelText: 'Nouveau mot de passe',
                                      labelStyle: TextStyle(
                                          color: Colors.black38,
                                          fontSize: 3 * SIZE),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1, color: Colors.black38)),
                                    )),
                                flex: 11,
                              )
                            ],
                          )
                        : Container(),
                    Divider(),
                    _user.id == _currentUser.id
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Notifications',
                                  style: TextStyle(fontSize: 3 * SIZE)),
                              Switch(
                                onChanged: (bool value) {
                                  setState(() {
                                    _user.enableNotification = value;
                                  });
                                  _user.sendUpdateUser();
                                },
                                value: _user.enableNotification,
                              ),
                            ],
                          )
                        : Container(),
                    _user.id == _currentUser.id
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Rappels',
                                  style: TextStyle(fontSize: 3 * SIZE)),
                              Switch(
                                onChanged: (bool value) {
                                  setState(() {
                                    _user.enableReminder = value;
                                  });
                                  _user.sendUpdateUser();
                                },
                                value: _user.enableReminder,
                              ),
                            ],
                          )
                        : Container(),
                  ],
                ),
              ),
            )));
  }

  void _validateForm() async {
    FocusScope.of(context).requestFocus(new FocusNode());

    await _currentUser.sendUpdateUser();
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();

      if (_oldPassword != "" && _newPassword != "") {
        // Verifier si l'ancien mot de passe est correcte
        final responseLLT = await http.post(
            '$AUTH_API/$REALM?email=${_currentUser.email}',
            headers: {'Content-Type': 'application/json'},
            body: json.encode({"pwd": _oldPassword}));
        if (responseLLT.statusCode == 200) {
          final response = await http.post(
              '$AUTH_API/${_currentUser.email}/change',
              headers: {'Content-Type': 'application/json'},
              body: json.encode(
                  {"llt_token": _currentUser.llt_token, "pwd": _newPassword}));
          if (response.statusCode == 200) {
            setState(() {
              _editing = false;
            });
            displaySnackBar(
                "Votre mot de passe vient d'être modifié avec succès",
                _scaffoldKey,
                isSuccess: true);
            return;
          }
        } else if (responseLLT.statusCode == 500) {
          displaySnackBar("Ancien mot de passe incorrect", _scaffoldKey,
              isSuccess: false);
          return;
        }
        displaySnackBar(
            "Impossible de se connecter à Lovli pour l'instant", _scaffoldKey,
            isSuccess: false);
      } else {
        setState(() {
          _editing = false;
        });
      }
    }
  }
}
