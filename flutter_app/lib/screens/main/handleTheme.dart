import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/utils/formsValidators.dart';
import 'package:flutter_app/widgets/cover.dart';
import '../../models/theme.dart' as model;
import 'dart:convert';
import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class HandleTheme extends StatefulWidget {
  final User currentUser;
  final List<model.Theme> themes;
  final model.Theme theme;
  final scaffoldKey;
  //final Future Function(String, String, String) showNotificationWithSound;
  final SocketIO themeSocket;

  const HandleTheme(
      {Key key,
      this.currentUser,
      this.scaffoldKey,
      this.themes,
      //this.showNotificationWithSound,
      this.themeSocket,
      this.theme})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => HandleThemeState();
}

class HandleThemeState extends State<HandleTheme>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool _isvalidTheme = true;
  List<model.Theme> _themes = [];
  model.Theme _theme;
  String _themeName;
  SocketIO _themeSocket;
  List<int> _cover;
  bool _pickedImage = false;

  @override
  void initState() {
    super.initState();
    _themes = widget.themes;
    _theme = widget.theme;
    _themeName = _theme != null ? _theme.name : '';
    _cover = _theme != null ? _theme.cover : null;
    _themeSocket = widget.themeSocket;
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  Future _pickImage(ImageSource imageSource) async {
    Navigator.pop(context);
    final File cover = await ImagePicker.pickImage(
        source: imageSource, maxWidth: 256, maxHeight: 256);
    if (cover != null) {
      setState(() {
        _cover = cover.readAsBytesSync();
        _pickedImage = true;
      });
    }
  }

  void _displayImagePickerChoices() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[
                  GestureDetector(
                    child: new Text("Ouvrir l'appareil photo"),
                    onTap: () => _pickImage(ImageSource.camera),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  GestureDetector(
                    child: new Text('Ouvrir la galérie'),
                    onTap: () => _pickImage(ImageSource.gallery),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return Center(
      child: SingleChildScrollView(
          child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(4 * SIZE),
              padding: EdgeInsets.all(5 * SIZE),
              height: (_pickedImage ? 65 : 60) * SIZE,
              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      autovalidate: _autoValidate,
                      child: TextFormField(
                        initialValue: _themeName,
                        validator: (value) => notEmptyValidator(
                            value, "Veuillez saisir le nom du thème"),
                        style: TextStyle(fontSize: 3 * SIZE),
                        onSaved: (String value) => _themeName = value,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.style,
                              color: Colors.orange,
                              size: 4 * SIZE,
                            ),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1, color: Colors.black38)),
                            hasFloatingPlaceholder: true,
                            labelText: 'Nom du thème',
                            labelStyle: TextStyle(
                                color: Colors.black38, fontSize: 3.5 * SIZE)),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 8 * SIZE, top: SIZE),
                      alignment: Alignment.centerLeft,
                      child: !_isvalidTheme
                          ? Text('Ce thème existe déjà',
                              style: TextStyle(
                                  fontSize: 2.5 * SIZE, color: Colors.red))
                          : null,
                    ),
                    SizedBox(
                      height: 3 * SIZE,
                    ),
                    _cover == null
                        ? GestureDetector(
                            onTap: _displayImagePickerChoices,
                            child: Container(
                              width: 20 * SIZE,
                              height: 20 * SIZE,
                              child: Icon(
                                Icons.add_a_photo,
                                size: 20 * SIZE,
                              ),
                            ),
                          )
                        : Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Cover(
                                cover: _cover,
                                width: 40 * SIZE,
                                defaultCover: 'assets/images/theme-icon.png',
                              ) /*Cover(
                    cover: event.cover,
                    defaultCover: 'assets/images/illustration.png',
                  )*/
                              ,
                              IconButton(
                                onPressed: _displayImagePickerChoices,
                                icon: Icon(Icons.edit),
                                color: Colors.black87,
                                iconSize: 7 * SIZE,
                                alignment: Alignment.topLeft,
                              )
                            ],
                          ),
                    SizedBox(
                      height: 3 * SIZE,
                    ),
                    CupertinoButton(
                      color: Colors.orange,
                      child: Text(
                        _theme == null ? "Créér" : "Modifier",
                        style: TextStyle(fontSize: 3 * SIZE),
                      ),
                      onPressed: _validateForm,
                      padding:
                          EdgeInsets.fromLTRB(20 * SIZE, SIZE, 20 * SIZE, SIZE),
                    )
                  ],
                ),
              )),
        ),
      )),
    );
  }

  void _validateForm() {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      //If all data are correct then save data to out variables
      _formKey.currentState.save();
      if ((_theme != null && _theme.name != _themeName) &&
          (_themes.map((theme) => theme.name).contains(_themeName))) {
        setState(() {
          _isvalidTheme = false;
        });
      } else {
        _theme == null ? _createTheme() : _updateTheme();
      }
    }
  }

  void _createTheme() {
    model.Theme(
            name: _themeName, createBy: widget.currentUser.id, cover: _cover)
        .createTheme()
        .then((theme) {
      _themeSocket.emit('create', [theme.toJson(theme.id)]);
      setState(() {
        _themes.add(theme);
      });
      Navigator.pop(context);
      displaySnackBar('Thème créé avec succès', widget.scaffoldKey,
          isSuccess: true);
    }).catchError((err) {
      print(err);
      displaySnackBar('Problème de connexion', widget.scaffoldKey,
          isSuccess: false);
    });
  }

  void _updateTheme() {
    setState(() {
      _theme.name = _themeName;
      _theme.cover = _cover;
    });
    _theme.updateTheme().then((editTheme) {
      final int index = _themes.indexWhere((event) => event.id == editTheme.id);
      _themeSocket.emit('edit', [editTheme.toJson(editTheme.id)]);
      setState(() {
        _themes.replaceRange(index, index + 1, [editTheme]);
      });
      Navigator.pop(context);
      displaySnackBar('Thème modifié avec succès', widget.scaffoldKey,
          isSuccess: true);
    }).catchError((err) {
      print(err);
      displaySnackBar('Problème de connexion', widget.scaffoldKey,
          isSuccess: false);
    });
  }
}
