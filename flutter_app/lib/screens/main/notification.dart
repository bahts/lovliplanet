import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import '../../models/user.dart';
import '../../models/notification.dart';
import '../../models/theme.dart' as screen;
import 'package:flutter_app/widgets/notifWidget.dart';

class NotificationScreen extends StatefulWidget {
  final User user;
  final List<Event> events;
  final List<screen.Theme> themes;

  NotificationScreen({this.user, this.events, this.themes});

  @override
  State<StatefulWidget> createState() {
    return NotificationState();
  }
}

class NotificationState extends State<NotificationScreen> {
  User _user;
  List<Event> _events;
  List<screen.Theme> _themes;

  @override
  void initState() {
    super.initState();
    _user = widget.user;
    _events = widget.events;
    _themes = widget.themes;
  }

  @override
  Widget build(BuildContext context) {
    final List<NotificationObj> notifications =
        _user.notifications.reversed.toList();

    final List<NotifWidget> notifList =
        List<NotifWidget>.from(notifications.map((notif) => NotifWidget(
              currentUser: _user,
              events: _events,
              notification: notif,
              themes: _themes,
            )));

    return notifList.isNotEmpty
        ? Container(
            color: Colors.white,
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: notifList.toList(),
              reverse: false,
            ),
          )
        : Center(
            child: Text('Pas de notifications'),
          );
  }
}
