import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/cover.dart';
import 'package:flutter_app/widgets/userWidget.dart';

import '../../models/user.dart';
import '../../models/event.dart';

class DisplayUsers extends StatelessWidget {
  final List<Future<User>> users;
  final List<Event> events;
  final User currentUser;

  const DisplayUsers({
    Key key,
    this.users,
    this.events,
    this.currentUser,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DisplayUsersFul(users, events, currentUser);
  }
}

class DisplayUsersFul extends StatefulWidget {
  final List<Future<User>> users;
  final List<Event> events;
  final User currentUser;

  const DisplayUsersFul(
    this.users,
    this.events,
    this.currentUser,
  );

  @override
  State<StatefulWidget> createState() {
    return DisplayEventsState();
  }
}

class DisplayEventsState extends State<DisplayUsersFul>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;

    final usersList = widget.users.map((userFuture) => FutureBuilder<User>(
          future: userFuture,
          builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
            if (snapshot.hasData) {
              User user = snapshot.data;
              return UserWidget(
                currentUser: widget.currentUser,
                events: widget.events,
                user: user,
              );
            } else if (snapshot.hasError) {
              print(snapshot.error);
            }
            return Container();
          },
        ));

    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
            scale: scaleAnimation,
            child: Container(
              margin: EdgeInsets.all(4 * SIZE),
              padding: EdgeInsets.all(SIZE),
              height: 120 * SIZE,
              decoration: ShapeDecoration(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0))),
              child: usersList.isNotEmpty
                  ? ListView(
                      scrollDirection: Axis.vertical,
                      padding: EdgeInsets.zero,
                      children: usersList.toList(),
                      reverse: false,
                    )
                  : Image.asset(
                      'assets/images/no-user.jpg',
                      scale: 0.7,
                    ),
            )),
      ),
    );
  }
}
