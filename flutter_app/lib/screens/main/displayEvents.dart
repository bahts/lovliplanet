import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/screens/main/handleTheme.dart';
import 'package:flutter_app/widgets/listEvent.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';
import '../../models/theme.dart' as model;

class DisplayEvents extends StatelessWidget {
  final List<model.Theme> themes;
  final User currentUser;
  final List<Event> events;
  final String title;
  final SocketIO themeSocket, eventSocket;
  final model.Theme theme;

  const DisplayEvents(
      {Key key,
      this.themes,
      this.currentUser,
      this.events,
      this.title,
      this.themeSocket,
      this.eventSocket,
      this.theme})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DisplayEventsFul(
        themes, currentUser, events, title, themeSocket, eventSocket, theme);
  }
}

class DisplayEventsFul extends StatefulWidget {
  final List<model.Theme> themes;
  final User currentUser;
  final List<Event> events;
  final String title;
  final SocketIO themeSocket, eventSocket;
  final model.Theme theme;

  const DisplayEventsFul(this.themes, this.currentUser, this.events, this.title,
      this.themeSocket, this.eventSocket, this.theme);

  @override
  State<StatefulWidget> createState() {
    return DisplayEventsState();
  }
}

class DisplayEventsState extends State<DisplayEventsFul> {
  List<model.Theme> _themes;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  User _currentUser;
  List<Event> _events;
  bool _isActive;
  SocketIO _themeSocket, _eventSocket;
  model.Theme _theme;

  @override
  void initState() {
    super.initState();
    _themes = widget.themes;
    _theme = widget.theme;
    _currentUser = widget.currentUser;
    _eventSocket = widget.eventSocket;
    _themeSocket = widget.themeSocket;
    _events = widget.events;
    _isActive = widget.theme != null
        ? widget.theme.subscribers.contains(widget.currentUser.id)
        : false;
  }

  void sortEvents(index) {
    switch (index) {
      case 0:
        _events.sort(
            (event1, event2) => event2.createAt.compareTo(event1.createAt));
        break;
      case 1:
        _events.sort((event1, event2) =>
            event2.viewers.length.compareTo(event1.viewers.length));
        break;
      case 2:
        _events.sort((event1, event2) =>
            event2.participants.length.compareTo(event1.participants.length));
        break;
      default:
        _events.sort(
          (event1, event2) => event1.createAt.compareTo(event2.createAt),
        );
        break;
    }
  }

  Widget _more(double size) {
    return Container(
      height: 14 * size,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (_) => HandleTheme(
                        currentUser: _currentUser,
                        scaffoldKey: _scaffoldKey,
                        theme: _theme,
                        themes: _themes,
                        themeSocket: _themeSocket,
                      ));
              _scaffoldKey.currentState.removeCurrentSnackBar();
            },
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.edit,
                  color: Colors.black,
                  size: 4 * size,
                ),
                SizedBox(
                  width: 4 * size,
                ),
                Text(
                  'Modifier',
                  style: TextStyle(fontSize: 3 * size, color: Colors.black),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          GestureDetector(
            onTap: () {
              _scaffoldKey.currentState.removeCurrentSnackBar();
            },
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.close,
                  color: Colors.black,
                  size: 4 * size,
                ),
                SizedBox(
                  width: 4 * size,
                ),
                Text(
                  'Annuler',
                  style: TextStyle(fontSize: 3 * size, color: Colors.black),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    sortEvents(0);
    return Scaffold(
      key: _scaffoldKey,
      body: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            actions: _theme == null
                ? <Widget>[]
                : <Widget>[
                    Switch(
                      value: _isActive,
                      onChanged: (bool value) {
                        value == true
                            ? _theme.subscribers.add(_currentUser.id)
                            : _theme.subscribers.remove(_currentUser.id);
                        setState(() {
                          _isActive = value;
                        });
                        _theme.updateTheme();
                      },
                    ),
                    _theme.createBy == _currentUser.id
                        ? IconButton(
                            onPressed: () {
                              final snackBar = SnackBar(
                                  duration: Duration(days: 1),
                                  backgroundColor: Colors.grey.shade100,
                                  content: _more(SIZE));
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            },
                            icon: Icon(Icons.more_horiz),
                            iconSize: 4 * SIZE,
                          )
                        : Container()
                  ],
            leading: GestureDetector(
              child: Icon(
                Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
            bottom: TabBar(
              onTap: (index) => sortEvents(index),
              labelColor: Colors.black,
              tabs: [
                Tab(
                    child: Text(
                  'Récents',
                  style: TextStyle(fontSize: 2.5 * SIZE),
                )),
                Tab(
                    child: Text(
                  'Vues',
                  style: TextStyle(fontSize: 2.5 * SIZE),
                )),
                Tab(
                    child: Text(
                  'Participants',
                  style: TextStyle(fontSize: 2.5 * SIZE),
                )),
              ],
            ),
            title: Text(
              widget.title,
              style: TextStyle(color: Colors.black),
            ),
          ),
          body: _events.length > 0
              ? TabBarView(
                  children: [
                    ListEvent(_events, _currentUser, _eventSocket,
                        _theme == null ? _themes : [_theme], null),
                    ListEvent(_events, _currentUser, _eventSocket,
                        _theme == null ? _themes : [_theme], null),
                    ListEvent(_events, _currentUser, _eventSocket,
                        _theme == null ? _themes : [_theme], null),
                  ],
                )
              : Center(
                  child: Text('Aucun évènement trouvé',
                      style: TextStyle(fontSize: 2.5 * SIZE))),
        ),
      ),
    );
  }
}
