import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/utils/date.dart';
import '../../models/user.dart';
import '../main/displayEvents.dart';
import '../../models/theme.dart' as model;
import 'package:adhara_socket_io/adhara_socket_io.dart';

class Search extends StatefulWidget {
  final User currentUser;
  final List<Event> events;
  final List<model.Theme> themes;
  final SocketIO themeSocket, eventSocket;

  Search(this.events, this.currentUser, this.themes, this.themeSocket,
      this.eventSocket);

  @override
  State<StatefulWidget> createState() {
    return SearchState();
  }
}

class SearchState extends State<Search> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _controller = TextEditingController();
  User _currentUser;
  List<Event> _events;
  String _keyword = '';
  String _location = '';
  List<String> _themesIds = [];
  DateTime _date = DateTime.now();
  TimeOfDay _time = TimeOfDay.now();
  SocketIO _themeSocket, _eventSocket;

  @override
  void initState() {
    super.initState();
    _currentUser = widget.currentUser;
    _events = widget.events;
    _eventSocket = widget.eventSocket;
    _themeSocket = widget.themeSocket;
  }

  @override
  void dispose() {
    // other dispose methods
    _controller.dispose();
    super.dispose();
  }

  Future _selectDate(BuildContext context) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    DateTime pickedDate = await showDatePicker(
        locale: const Locale('fr', 'FR'),
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2019),
        lastDate: DateTime(2025));

    if (pickedDate != null) setState(() => _date = pickedDate);
  }

  Future _selectTime(BuildContext context) async {
    TimeOfDay pickedTime =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());

    if (pickedTime != null) setState(() => _time = pickedTime);
  }

  @override
  Widget build(BuildContext context) {
    final double SIZE = MediaQuery.of(context).size.width / 82;
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.fromLTRB(2 * SIZE, 2 * SIZE, 2 * SIZE, 0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Container(
                height: 8 * SIZE,
                child: ListView.builder(
                  itemCount: widget.themes.length,
                  reverse: false,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (_, int index) => Card(
                        child: RaisedButton(
                          color: _themesIds.contains(widget.themes[index].id)
                              ? Colors.orange.shade400
                              : Colors.grey.shade100,
                          child: Text(
                            widget.themes[index].name,
                            style: TextStyle(fontSize: 2.5 * SIZE),
                          ),
                          onPressed: () {
                            if (_themesIds.contains(widget.themes[index].id)) {
                              setState(() {
                                _themesIds.remove(widget.themes[index].id);
                              });
                            } else {
                              setState(() {
                                _themesIds.add(widget.themes[index].id);
                              });
                            }
                          },
                        ),
                      ),
                ),
              ),
              SizedBox(
                height: 20 * SIZE,
              ),
              TextFormField(
                style: TextStyle(fontSize: 3 * SIZE),
                onSaved: (String value) => _keyword = value,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    icon: Icon(
                      Icons.keyboard,
                      color: Colors.orange,
                      size: 4 * SIZE,
                    ),
                    focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(width: 1, color: Colors.black38)),
                    hasFloatingPlaceholder: true,
                    labelText: 'Mot clé',
                    labelStyle:
                        TextStyle(color: Colors.black38, fontSize: 3.1 * SIZE)),
              ),
              SizedBox(
                height: SIZE,
              ),
              TextFormField(
                style: TextStyle(fontSize: 3 * SIZE),
                onSaved: (String value) => _location = value,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    icon: Icon(
                      Icons.location_on,
                      color: Colors.orange,
                      size: 4 * SIZE,
                    ),
                    focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(width: 1, color: Colors.black38)),
                    hasFloatingPlaceholder: true,
                    labelText: 'Lieu',
                    labelStyle:
                        TextStyle(color: Colors.black38, fontSize: 3.1 * SIZE)),
              ),
              SizedBox(
                height: SIZE,
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        primaryColor: Colors.orange, //color of the main banner
                      ),
                      child: Builder(
                          builder: (BuildContext context) => GestureDetector(
                                onTap: () => _selectDate(context),
                                behavior: HitTestBehavior.opaque,
                                child: TextFormField(
                                  enabled: _selectDate == null,
                                  decoration: InputDecoration(
                                      icon: Icon(
                                        Icons.date_range,
                                        color: Colors.orange,
                                        size: 4 * SIZE,
                                      ),
                                      labelText: '${formatEventDate(_date)}',
                                      labelStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 3.1 * SIZE)),
                                ),
                              )),
                    ),
                  ),
                  Flexible(
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        primaryColor: Colors.orange, //color of the main banner
                      ),
                      child: Builder(
                          builder: (BuildContext context) => GestureDetector(
                                onTap: () => _selectTime(context),
                                behavior: HitTestBehavior.opaque,
                                child: TextFormField(
                                  enabled: _selectDate == null,
                                  decoration: InputDecoration(
                                      icon: Icon(
                                        Icons.date_range,
                                        color: Colors.orange,
                                        size: 4 * SIZE,
                                      ),
                                      labelText: '${formatEventTime(_time)}',
                                      labelStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 3.1 * SIZE)),
                                ),
                              )),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 9 * SIZE,
              ),
              CupertinoButton(
                color: Colors.orange,
                child: Text(
                  "Rechercher",
                  style: TextStyle(fontSize: 3 * SIZE),
                ),
                onPressed: _validateForm,
                padding: EdgeInsets.fromLTRB(20 * SIZE, 0, 20 * SIZE, 0),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _validateForm() {
    _formKey.currentState.save();
    final dateTime =
        DateTime(_date.year, _date.month, _date.day, _time.hour, _time.minute);

    List<Event> cloneEvents = List<Event>.from(_events); // une copie
    cloneEvents
        .removeWhere((event) => event.date <= dateTime.millisecondsSinceEpoch);
    if (_themesIds.length != 0)
      cloneEvents.removeWhere((event) => !_themesIds.contains(event.theme));
    if (_keyword != '')
      cloneEvents.removeWhere((event) => !(event.title + event.description)
          .toLowerCase()
          .contains(_keyword.toLowerCase()));
    if (_location != '')
      cloneEvents.removeWhere((event) =>
          !event.location.toLowerCase().contains(_location.toLowerCase()));
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => DisplayEvents(
                eventSocket: _eventSocket,
                themeSocket: _themeSocket,
                theme: null,
                themes: widget.themes,
                currentUser: _currentUser,
                events: cloneEvents,
                title: 'Recherche',
              ),
        ));
  }
}
