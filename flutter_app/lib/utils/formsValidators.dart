import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';

void displaySnackBar(String message, GlobalKey<ScaffoldState> scaffoldKey,
    {bool isSuccess = false, num duration = 2}) {
  final snackBar = SnackBar(
      duration: Duration(seconds: duration),
      backgroundColor: isSuccess ? Colors.green : Colors.red,
      content: Text(
        message,
      ));
  scaffoldKey.currentState.showSnackBar(snackBar);
}

String generateMd5(String input) {
  return md5.convert(utf8.encode(input)).toString();
}

//final String formatEmailValid = r'\w+\d*\@\.+\.com';

String validateEmail(String value) {
  //RegExp regEmail = RegExp(formatEmailValid);
  if (value.contains('@')) {
    return null;
  } else {
    return 'email invalide: prenom.nom@orange.com';
  }
}

String validatePassword(String value) {
  if (value.length > 0) {
    return null;
  } else {
    return 'Veuillez saisir votre mot de passe';
  }
}

String notEmptyValidator(String value, String message) {
  if (value.length > 0) {
    return null;
  }
  return message;
}
