import 'package:flutter/material.dart';

String checkFormat(int n) => '${n < 10 ? '0' : ''}$n';

String getTimeFromNow(num date) {
  var diff = DateTime.now()
      .difference(DateTime.fromMillisecondsSinceEpoch(date, isUtc: true));
  if (diff.inDays > 0) {
    return "il y a ${diff.inDays}j";
  } else if (diff.inHours > 0) {
    return "il y a ${diff.inHours}h";
  } else if (diff.inMinutes > 0) {
    return "il y a ${diff.inMinutes}min";
  } else if (diff.inSeconds > 0) {
    return "il y a ${diff.inSeconds}s";
  }
  return "maintenant";
}

String formatEventDate(DateTime date) =>
    "${checkFormat(date.day)}/${checkFormat(date.month)}/${date.year}";

String formatEventTime(TimeOfDay time) =>
    "${time.hour}:${checkFormat(time.minute)}";
