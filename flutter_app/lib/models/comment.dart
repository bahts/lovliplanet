import 'dart:convert';

import 'package:flutter_app/models/event.dart';
import 'package:flutter_app/utils/contants.dart';
import 'package:http/http.dart' as http;

class Comment {
  final String id;
  final String event;
  final String author;
  final String content;
  final num createAt;

  Comment({
    this.id,
    this.event,
    this.author,
    this.content,
    this.createAt,
  });

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json["_id"],
        event: json["event"],
        author: json["author"],
        content: json["content"],
        createAt: json["createAt"],
      );

  Map<String, dynamic> toJson(String id) {
    Map<String, dynamic> myJson = {
      "_id": id,
      "event": event,
      "author": author,
      "content": content,
      "createAt": createAt,
    };
    if (id == null) myJson.remove("_id");
    return myJson;
  }

  Future<Event> addComment(String idEvent) async {
    final response = await http.post('$DOMAIN/events/$idEvent/comments',
        headers: {'Content-Type': 'application/json'},
        body: json.encode(this.toJson(null)));
    if (response.statusCode == 200) {
      return Event.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur d'enregistrement du commentaire");
  }
}
