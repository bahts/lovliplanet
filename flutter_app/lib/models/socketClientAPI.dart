import 'dart:async';
import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:flutter_app/utils/contants.dart';

class SocketClientAPI {
  final SocketIOManager manager;
  final String path;

  SocketClientAPI(this.path, this.manager);

  Future<SocketIO> initSocket() async {
    Completer<bool> isConnected = Completer<bool>();
    Map<String, String> query = Map<String, String>();

    query['transport'] = 'websocket';
    SocketIO _socket;
    _socket = await manager.createInstance(
      DOMAIN + path,
      query: query,
      enableLogging: false,
    );

    _socket.onConnect((_) {
      print("connected to $path socket...");
      isConnected.complete(true);
    });

    _socket.onConnectError((data) {
      print("error connected...");
      print(data);
    });
    _socket.onConnectTimeout((data) {
      print("timeout connected...");
      print(data);
    });
    _socket.onError((data) {
      print("error...");
      print(data);
      isConnected.complete(false);
    });
    _socket.onDisconnect((data) {
      print("disconnected...");
      print(data);
    });

    _socket.connect();

    return _socket;
  }
}
/*import 'dart:async';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:flutter_app/utils/contants.dart';

class SocketClientAPI {
  final String path;

  IO.Socket _socket;

  SocketClientAPI(this.path);

  Future<bool> handshake(String token) {
    Completer<bool> isConnected = Completer<bool>();
    Map<dynamic, dynamic> options = Map<dynamic, dynamic>();

    options['forceNew'] = true;
    options['autoConnect'] = true;
    options['query'] = {
      'accessToken': token,
    };

    _socket = IO.io(DOMAIN, options);

    _socket.on('connect', (_) {
      print('connect');
      print('id: ${_socket.id}');
      isConnected.complete(true);
    });
    _socket.on('disconnect', (_) => print('disconnect'));

    _socket.on('error', (error) {
      print('error');
      print(error);
      isConnected.complete(false);
    });

    _socket.open();

    print(_socket.opts);
    print(_socket.query);
    print(_socket.json);

    print('Connection: ${_socket.connected.toString()}');
    print('Disconnection: ${_socket.disconnected.toString()}');

    return isConnected.future;
  }
}*/
