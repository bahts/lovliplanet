import 'dart:convert';
import 'package:flutter_app/utils/contants.dart';
import 'package:http/http.dart' as http;
import './user.dart';

class NotificationObj {
  String id;
  String genre;
  String content;
  String target;
  bool viewed;
  num createAt;

  NotificationObj(
      {this.id,
      this.genre,
      this.target,
      this.content,
      this.viewed,
      this.createAt});

  factory NotificationObj.fromJson(Map<String, dynamic> json) =>
      NotificationObj(
        id: json["_id"],
        genre: json["genre"],
        content: json["content"],
        target: json["target"],
        viewed: json["viewed"],
        createAt: json["createAt"],
      );

  String toJson(String id) {
    Map<String, dynamic> myJson = {
      "_id": id,
      "genre": genre,
      "content": content,
      "viewed": viewed,
      "target": target,
      "createAt": createAt
    };
    if (id == null) myJson.remove("_id");
    return json.encode(myJson);
  }

  Future<List<NotificationObj>> pushNotification(String userId) async {
    final response = await http.post('$DOMAIN/users/$userId/notifications',
        headers: {'Content-Type': 'application/json'}, body: this.toJson(null));
    if (response.statusCode == 200) {
      return User.fromJson(json.decode(response.body)).notifications;
    }
    throw Exception("Erreur d'enregistrement du commentaire");
  }

  Future<bool> viewedNotification(String userId) async {
    final response = await http.put('$DOMAIN/users/$userId/notifications/$id');
    return response.statusCode == 200;
  }
}
