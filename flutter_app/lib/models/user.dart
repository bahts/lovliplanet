import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import './notification.dart';
import '../utils/contants.dart';

class User {
  String id;
  String email;
  String login;
  String llt_token;
  String jwt_token;
  List<int> avatar;
  List<String> eventsPart;
  List<String> eventsCreated;
  List<String> themesSub;
  List<String> eventsViewed;
  List<NotificationObj> notifications;
  bool isAdmin;
  bool enableNotification;
  bool enableReminder;
  num createAt;

  User(
      {this.id,
      this.email,
      this.login,
      this.llt_token = "",
      this.jwt_token = "",
      this.avatar = const [],
      this.notifications = const [],
      this.isAdmin = true,
      this.enableNotification = true,
      this.enableReminder = true,
      this.createAt});

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["_id"],
        email: json["email"],
        login: json["login"],
        llt_token: json["llt_token"],
        jwt_token: json["jwt_token"],
        avatar: base64Decode(json["avatar"]),
        notifications: List<NotificationObj>.from(json["notifications"]
            .map((notifJson) => NotificationObj.fromJson(notifJson))),
        isAdmin: json["isAdmin"],
        enableNotification: json["enableNotification"],
        enableReminder: json["enableReminder"],
        createAt: json["createAt"],
      );

  String toJson() => json.encode({
        "email": email,
        "login": login,
        "llt_token": llt_token,
        "jwt_token": jwt_token,
        "avatar": base64Encode(avatar),
        "notifications":
            notifications.map((notif) => notif.toJson(notif.id)).toList(),
        "isAdmin": isAdmin,
        "enableNotification": enableNotification,
        "enableReminder": enableReminder
      });

  Future createUser() async {
    final response = await http.post('$DOMAIN/users',
        headers: {'Content-Type': 'application/json'}, body: this.toJson());
    if (response.statusCode == 200) {
      return null;
    }
    throw Exception("Erreur de creation de l'utilisateur");
  }

  static Future<Map<String, dynamic>> addUserToLovli(
      String email, String login, String password) async {
    final response = await getUserByEmail(email);
    if (response.length == 0) {
      final responseCreate = await http.post(
          '$AUTH_API/manage/$REALM/create/$email',
          headers: {'Content-Type': 'application/json'},
          body: json.encode({"login": login, "pwd": password}));

      print(responseCreate.statusCode);
      if (responseCreate.statusCode == 201) {
        // la creation a ete realisé avec succès
        return <String, dynamic>{
          "successful": true,
          "message":
              "Nous venons de vous inscrire aux services Lovli. Pour poursuivre, activez votre compte en cliquant sur le lien qui vient de vous être envoyé par mail."
        };
      } else if (responseCreate.statusCode == 403) {
        // l'utlilisateur est dejà inscrit sur Lovli
        final responseLLT = await http.post('$AUTH_API/$REALM?email=$email',
            headers: {'Content-Type': 'application/json'},
            body: json.encode({"pwd": password}));
        print(responseLLT.statusCode);
        if (responseLLT.statusCode == 200) {
          final Map<String, dynamic> userLLT = json.decode(responseLLT.body);
          print(userLLT);
          final responseJWT = await http.post(
              '$AUTH_API/$email/jwt/$MAIN_REALM',
              headers: {'Content-Type': 'application/json'},
              body: json.encode({'llt_token': userLLT['llt_token']}));
          if (responseJWT.statusCode == 200) {
            final Map<String, dynamic> userJWT = json.decode(responseJWT.body);
            print(userJWT);
            final responseLink = await http.post(
              '$AUTH_API/manage/$REALM/link/$email?login=$login',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ${userJWT['jwt']}'
              },
            );
            if (responseLink.statusCode == 201) {
              return <String, dynamic>{
                "successful": true,
                "message":
                    "Vous êtes déjà inscrit à Lovli, veuillez utiliser le même mot de passe."
              };
            }
          }
        }
      }
      throw Exception("Erreur de creation de l'utilisateur");
    }

    return <String, dynamic>{
      "successful": false,
      "message":
          "Vous êtes déjà inscrit dans notre base de données, tentez de vous connecter."
    };
  }

  static Future<Map<String, dynamic>> loginToLovli(
      String email, String password) async {
    final responseLLT = await http.post('$AUTH_API/$REALM?email=$email',
        headers: {'Content-Type': 'application/json'},
        body: json.encode({"pwd": password}));
    return <String, dynamic>{
      "successful": responseLLT.statusCode == 200,
      "data": json.decode(responseLLT.body)
    };
  }

  static Future lostPassword(String email) async {
    final response =
        await http.post('$AUTH_API/manage/$REALM/ask_pwd_reset_token/$email');
    if (response.statusCode == 200) {
      return null;
    }
    throw Exception("Probleme de connexion internet");
  }

  static Future<List<dynamic>> getUserByEmail(String email) async {
    final response = await http.post('$DOMAIN/auth',
        headers: {'Content-Type': 'application/json'},
        body: json.encode({"email": email}));
    print(response.body);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    }
    throw Exception("Erreur de creation de l'utilisateur");
  }

  static Future<User> getUser(String id) async {
    final response = await http.get('$DOMAIN/users/$id');
    if (response.statusCode == 200) {
      return User.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur de recuperation de l'utilisateur");
  }

  Future sendUpdateUser() async {
    final response = await http.put('$DOMAIN/users/$id',
        headers: {'Content-Type': 'application/json'}, body: this.toJson());
    if (response.statusCode == 200) {
      return null;
    }
    throw Exception("Erreur de modification de l'utilisateur");
  }
}
