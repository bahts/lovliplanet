import 'dart:convert';
import 'package:flutter_app/utils/contants.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Theme {
  String id;
  String name;
  List<int> cover;
  List<String> subscribers;
  String createBy;
  num createAt;

  Theme({
    this.id,
    this.name,
    this.cover,
    this.subscribers = const [],
    this.createBy,
    this.createAt,
  });

  factory Theme.fromJson(Map<String, dynamic> json) => Theme(
        id: json["_id"],
        name: json["name"],
        cover: base64Decode(json["cover"]),
        subscribers:
            List<String>.from(json["subscribers"].map((userId) => userId)),
        createBy: json["createBy"],
        createAt: json["createAt"],
      );

  String toJson(String id) {
    Map<String, dynamic> myJson = {
      "_id": id,
      "name": name,
      "cover": cover != null ? base64Encode(cover) : '',
      "subscribers": subscribers.map((userId) => userId).toList(),
      "createBy": createBy,
      "createAt": createAt,
    };
    if (id == null) {
      myJson.remove("_id");
    }
    return json.encode(myJson);
  }

  static Future<List<Theme>> getThemes() async {
    final response = await http.get('$DOMAIN/themes');
    if (response.statusCode == 200) {
      return List<Theme>.from(json
          .decode(response.body)
          .map((themeJson) => Theme.fromJson(themeJson)));
    }
    throw Exception("Erreur de recuperation des thèmes");
  }

  static Future<Theme> getTheme(String id) async {
    final response = await http.get('$DOMAIN/themes/$id');
    if (response.statusCode == 200) {
      return Theme.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur de recuperation du thème");
  }

  Future<Theme> createTheme() async {
    final response = await http.post('$DOMAIN/themes',
        headers: {'Content-Type': 'application/json'}, body: this.toJson(null));
    print(json.decode(response.body));
    if (response.statusCode == 200) {
      return Theme.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur de creation du theme");
  }

  Future<Theme> updateTheme() async {
    final response = await http.put('$DOMAIN/themes/$id',
        headers: {'Content-Type': 'application/json'}, body: this.toJson(null));
    if (response.statusCode == 200) {
      return Theme.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur de modification du theme");
  }

  Future<bool> deleteTheme() async {
    final response = await http.delete('$DOMAIN/themes/$id');
    return response.statusCode == 200;
  }
}
