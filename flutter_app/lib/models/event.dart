import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import './comment.dart';
import '../utils/contants.dart';

class Event {
  String id;
  String title;
  String theme;
  String description;
  num date;
  String author;
  List<int> cover;
  String location;
  List<String> viewers;
  List<String> participants;
  List<Comment> comments;
  num createAt;

  Event(
      {this.id,
      this.title,
      this.theme,
      this.description,
      this.date,
      this.author,
      this.cover,
      this.location,
      this.viewers = const [],
      this.participants = const [],
      this.comments = const [],
      this.createAt});

  factory Event.fromJson(Map<String, dynamic> json) {
    return Event(
      id: json["_id"],
      title: json["title"],
      theme: json["theme"],
      description: json["description"],
      date: json["date"],
      author: json["author"],
      cover: base64Decode(json["cover"]),
      location: json["location"],
      viewers: List<String>.from(json["viewers"].map((userId) => userId)),
      participants:
          List<String>.from(json["participants"].map((userId) => userId)),
      comments: List<Comment>.from(
          json["comments"].map((commentJson) => Comment.fromJson(commentJson))),
      createAt: json["createAt"],
    );
  }

  Map<String, dynamic> toJson(String id) {
    Map<String, dynamic> myJson = {
      "_id": id,
      "title": title,
      "theme": theme,
      "description": description,
      "date": date,
      "author": author,
      "cover": cover != null ? base64Encode(cover) : '',
      "location": location,
      "viewers": viewers.map((userId) => userId).toList(),
      "comments":
          comments.map((comment) => comment.toJson(comment.id)).toList(),
      "participants": participants.map((userId) => userId).toList(),
      "createAt": createAt,
    };
    print(myJson);
    if (id == null) {
      myJson.remove("_id");
      myJson.remove("createAt");
    }

    return myJson;
  }

  static Future<List<Event>> getEvents(int page, int limit) async {
    final response = await http.get('$DOMAIN/events?page=$page&limit=$limit');
    if (response.statusCode == 200) {
      return List<Event>.from(json
          .decode(response.body)
          .map((eventJson) => Event.fromJson(eventJson)));
    }
    throw Exception("Erreur de chargement des evenements");
  }

  Future<Event> createEvent() async {
    final response = await http.post('$DOMAIN/events',
        headers: {'Content-Type': 'application/json'},
        body: json.encode(this.toJson(null)));
    if (response.statusCode == 200) {
      return Event.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur de creation de l'evenement");
  }

  Future<Event> sendUpdateEvent() async {
    Map<String, dynamic> myJson = this.toJson(null);
    myJson.remove("comments");
    final response = await http.put('$DOMAIN/events/$id',
        headers: {'Content-Type': 'application/json'},
        body: json.encode(myJson));
    if (response.statusCode == 200) {
      return Event.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur de modification cet evenement");
  }

  Future<bool> deleteEvent() async {
    final response = await http.delete('$DOMAIN/events/$id');
    return response.statusCode == 200;
  }

  Future<Event> updateViewers(String userId) async {
    final response = await http.put('$DOMAIN/events/$id/handleViews/$userId');
    if (response.statusCode == 200) {
      return Event.fromJson(json.decode(response.body));
    }
    throw Exception("Erreur lors de l'ajout d'un 'voyeur :-)' evenement");
  }

  Future<Event> updateParticipants(String userId, bool willParticipate) async {
    final response = await http.put(
        '$DOMAIN/events/$id/handleParticipants/$userId',
        headers: {'Content-Type': 'application/json'},
        body: json.encode({"willParticipate": willParticipate}));
    if (response.statusCode == 200) {
      return Event.fromJson(json.decode(response.body));
    }
    throw Exception(
        "Erreur lors de la modification des participants de cet evenement");
  }
}
