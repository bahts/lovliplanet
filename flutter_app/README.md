# LovliPlanet
## Frontend : Flutter

Le développement Front-end a été réalisé avec Flutter, une framework dart développer par Google.

### Authentification

La partie authentification est réalisé dans la setion **flutter_app/lib/screens/auth/**. Cette partie comporte 4 fichiers correspondants respectivements aux 4 écrans suivant:

*  **welcome.dart**: la page d'accueil de l'application
*  **login.dart**: la page de connexion
*  **signup.dart**: la page d'inscription
*  **forgotpassword.dart**: page de récupération de mot de passe

L'authentification est réalisé via une API externe à l'application afin de permettre aux utilisateurs de s'inscrire une seule fois sur la plateforme Lovli et de se connecter à toutes les applications Lovli.<br/>
Lorsqu'un utilisateur s'inscrit, un mail lui est envoyé, afin de confirmer son mail, avec un lien de confirmation. Après avoir cliqué sur lien, la création du compte sera effective et l'autilisateur en question pourra pourra se connecter à n'importe quels outils ou applications Lovli.<br/>
L'API d'authentification a été développer par Mr Gäetan Montury et son équipe qu'on remercie. Vous trouverez la documentation complète de cette API [ici](<https://lovli-auth-api.decoderoom.ovh/v1/auth>).<br/>

### Ecrans de l'application
L'application comporte 10 écrans, hors écrans d'authentification, qui se trouvent dans la section **flutter_app/lib/screens/main/**, et qui sont :

*  **agenda.dart**: la page affichant l'agenda de l'utilisateur dans laquelle se trouve les évènements aux quels il participe.
*  **dashboard.dart**: le tableau de bord de l'application; la page sur laquelle s'affiche les évènements, à venir, aux quels les utilisateurs peuvent partciper (la file d'actualité).
*  **eventDetail.dart**: la page affichant les déatail d'une application (description, lecture et post de commentaires, ...).
*  **handleEvent.dart**: le formulaire permettant de publier ou modifier un évènement.
*  **handleThemes.dart**: le formulaire permettant de créer ou modifier un thème
*  **notification.dart**: la page sur laquelle s'affiche les notifications liées à un utilisateur.
*  **profil.dart**: la page permettant d'afficher ou modifier les informations d'un utilisateur (avatar, pseudo, email, ...).
*  **search.dart**: la page affichant le formulaire de recherche d'un évènement.
*  **displayEvents.dart**: la page affichant la liste qu'on lui passe
*  **displayUsers.dart**: la page affichant la liste des utilisaturs qu'on lui passe

### Evènements
Les évènements sont l'essence de l'application, un évènement est publié par un utilisateur avec le droit **Admin**. L'évènement est lié à un thème qui peut être créé par un utilisateur **Admin**.<br/>
L'application offre la possiblité de laisser un commentaire sur évènement qui nous interesse, et participer ou non à à celui-ci.<br/>
La représentation d'un évènement de la file d'actualité se trouve dans le fichier **flutter_app/lib/widget/event**.<br/>
Les évènements de l'application sont chargés successivement pour ne pas avoir un temps de chargement très important en voulant charger tous les évènements d'un coup avant de les afficher.

### Notifications
Les notifications sont gérés en local, en effet nous avons utiliser des sockets (socket.io) afin de transmettre des informations en temps réel de l'utilisateur au serveur et vice-versa.<br/>
Grâce à cette transmission en temps réel, lorsqu'un évènement ou thème est créé ou modifié alors l'utilisateur emet un évènement au serveur lui notifiant que quelque chose vient de se passer.<br/> Le serveur à son tour emet un évènement aux autres utilisateurs, connectés, de l'appplication leur notifiant que quelque chose vient de se passer et ceux-ci publie une notification localement.<br/>
Ce qui fait qu'un élément est ajouté ou mis à jour dans l'application sans avoir à rafraîchir ou recharger toute l'application.

### Images
Les images de l'application sont converties en **base64** avant d'être sauvegarder dans la base de données car y aurait trop de conversion, entre le backend et le frontent, à réaliser s'il fallait sauvegarder en **binaire**.

#### Travaux non réalisés

*  Réalisation de groupe discussion entre les utilisateurs.
*  Notification aux utilisateurs qui participent à un évènement le jour-j du déroulement de celui-ci ; le faire avec la notification local.
*  Affichage des évènements non lus avant ceux lus dans la file d'actualité (pour l'instant les évènements les plus récents s'affichent en premier).
*  Sauvegarder les évènenemts aux quels participent un utilisateur dans l'agenda de son appareil. Fait à 80% dans le fichier **flutter_app/lib/screens/main/eventDetail.dart** ; faire attention au fait que l'appareil contienne que plusieurs agendas du coup se demander lequel choisir. [Ceci](<https://itnext.io/using-device-calendar-library-in-flutter-to-communicate-with-android-ios-calendar-95b2d8c77b40>) pourrait bien aider.
*  Pour l'instant un utilisateur est crée, pour le bien du déroulement des tests, avec le droit **Admin** alors ne pas oublier de le rectifier avant le déploiement.
